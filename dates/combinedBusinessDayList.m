function businessDayListNew = combinedBusinessDayList(businessDayListUSD, businessDayListSEK, businessDayListEUR)

 businessDayListNew =[];
for iter=1:length(businessDayListUSD)
    rowEUR = find(businessDayListEUR==businessDayListUSD(iter));
    rowUSD = iter;
    rowSEK = find(businessDayListSEK==businessDayListUSD(iter));
    
    if not(isempty(rowEUR)) && not(isempty(rowSEK))
         businessDayListNew =[businessDayListNew ; businessDayListUSD(iter) rowUSD rowEUR rowSEK ];
    end
end

end