function [timeCount numberOfPayments]= timeCountAndNumberOfPayments(valuationDate, paymentDates)
% Calculates the time to next payment for FRA and IRS
% Calculates the number of payments left in the IRS/FRA contract
%
% Inputs
%   dateZero - the starting date
%   timePayments - a vector with dates for all payments
%
% Outputs
%   timeCount - a vector with the remaining time to all future payments. Has
%   the same size as timePayments
%   numberOfPayments - the number of payments left in the IRS/FRA contract

%------------- BEGIN CODE --------------

for i = 1:length(paymentDates)
    timeToAllPayments(i) = (paymentDates(i) - valuationDate)/360;
end

timeCountFixed = timeToAllPayments(timeToAllPayments>0)
numberOfPayments = length(timeCountFixed)

end

%------------- END CODE --------------

