function numberOfDayCount = calculateNumberOfBusinessDayBetweenDatesImproved(dateOne, dateTwo, dateVector)

dateVector = evalin('base', 'businessDayImprovedList');

if (dateOne < 726835 || dateTwo < 726835)
  error('Dates is outside range.');
elseif dateOne <= 735902 && dateTwo <= 735902
  numberOfDayCount = abs(dateVector(dateOne-726834,2)-dateVector(dateTwo-726834,2));
elseif dateOne <= 735902
  numberOfDayCount = abs(dateVector(dateOne-726834,2)-(dateTwo-735902)-6217);
elseif dateTwo <= 735902
  numberOfDayCount = abs(dateVector(dateTwo-726834,2)-(dateOne-735902)-6217);
else
  numberOfDayCount = abs(dateTwo-dateOne);
end

end