classdef Portfolio
    %INSTRUMENT Base class for portfolios
    
    properties
        % member data
        instrumentArray; %Cell array of instrument objects
        priceVector; %Vector containing the portfoliovalue each work day
       %generalizedMethodVar; % Vector containing the VAR measure for each work day
        startDate; %start date for the portfolio as datenum
        endDate; %end date as datenum
    end
    
    methods
        function portfolio = Portfolio(iA, pV, sD, eD)
            portfolio.instrumentArray = iA;
            portfolio.priceVector = pV;
            %portfolio.generalizedMethodVar = zeros(length(pV,1));
            portfolio.startDate = sD;
            portfolio.endDate = eD;
        end
		
		function activeInstrumentList = getActiveInstrumentList(obj, date)
			totalNumberOfInstrument = length(instrumentArray);
			activeInstrumentList = {};
			activeInstrumentInterator = 1;
			
			for i=1:totalNumberOfInstrument
				if obj.instrumentArray(i).isActive(date)
					activeInstrumentList(activeInstrumentInterator) = obj.instrumentArray(i);
					activeInstrumentInterator = activeInstrumentInterator + 1;
				end
			end
			
			if isempty(activeInstrumentList)
				error('The portfolio does not have any active contracts on the specific date');
			end
		end
			
    end
end