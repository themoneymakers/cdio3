function priceOfIrs = priceIrs(elementIrs, LIBOR3M, valuationNumber, forwardMatrix, valuationDate)
% Price IRS at valuationDate
% Recieves floating rate and pays fixed rate
%
% Inputs   
%   elementIrs - element containing information about IRS
%       elementIrs.issueDate (scalar)
%       elementIrs.fixDates (vector)
%       elementIrs.floatDates (vector)
%       elementIrs.yieldFix (scalar)
%       elementIrs.nominalValue (scalar)
%   LIBOR3M (Container) - simple three months libor rates for all dates
%   valuationNumber [double] - the number corresponding to right date in discountMatrix
%   discountMatrix [matrix] - matrix with all discount rates
%   valuationDate [double] - the date the FRA should be valued
%
% Outputs
%   priceIRS [vector] - the price/value of each IRS contract (elementIrs) at valuationDate
%
%
%------------- BEGIN CODE ----------------------------------------------

numberOfInstruments = length(elementIrs);
priceOfIrs = zeros(numberOfInstruments,1);

for j = 1:numberOfInstruments

nominalValue = elementIrs{j}.nominalValue;
yieldFix = elementIrs{j}.yield;
issueDate = elementIrs{j}.issueDate;
spotDate = elementIrs{j}.spotDate;
fixDates = elementIrs{j}.fixDates;
floatDates = elementIrs{j}.floatDates;
short = elementIrs{j}.short;
maturityDate = elementIrs{j}.maturityDate;

% ----------------------------------------------------------------------
% Price of floating leg
if (maturityDate > valuationDate)
[timeCountFloat timeForNextFloatingPayment timeForLastFixing] = calculateTimeCountFloat(valuationDate, maturityDate, floatDates);
discountRateFloat = findDiscountRate(valuationNumber, forwardMatrix, spotDate, valuationDate, timeForNextFloatingPayment);

discountRateFloat= 0;


if (timeForLastFixing == 0)
    timeForLastFixing = issueDate;
end

yieldFloat = LIBOR3M(timeForLastFixing);

priceFloat = nominalValue*(1+yieldFloat*timeCountFloat)*exp(-discountRateFloat);

% -------------------------------------------
% Price of fixed leg
[timeCountFix timeForFixPayments numberOfFixPayments] = calculateTimeCountFix(valuationDate, spotDate, fixDates);

sumFixedCouponsTemp = 0;

for k = 1:numberOfFixPayments
    discountRateFix = findDiscountRate(valuationNumber, forwardMatrix, spotDate, valuationDate, timeForFixPayments(k));
    sumFixedCouponsTemp(k) = nominalValue*yieldFix*timeCountFix(k)*exp(-discountRateFix);
end

sumFixedCoupons=sum(sumFixedCouponsTemp);
discountRateFix = findDiscountRate(valuationNumber, forwardMatrix, spotDate, valuationDate, timeForFixPayments(numberOfFixPayments));

priceFix = sumFixedCoupons + nominalValue*exp(-discountRateFix);

% ----------------------------------------
% Price IRS
if short == 0
    priceOfIrs(j) = priceFloat - priceFix;
elseif short == 1
    priceOfIrs(j) = -(priceFloat - priceFix);
end


else
    priceOfIrs(j) = 0;
end

end
end

%------------- END CODE ----------------------------------------------

function [timeCountFloat timeForNextFloatingPayment timeForLastFixing] = calculateTimeCountFloat(valuationDate, maturityDate, paymentDates)
% Calculates needed date and time for floating leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] - containing dates for all floating payments
%
% Outputs
%   timeCountFloat - Proper time count for next floating payment
%   timeForNextFloatingPayment - The date of the next floating payment
%   timeForLastFixing - The date of the fixing of the floating rate 

%------------- BEGIN CODE -----------------------------------------------

for i = 1:length(paymentDates)
    timeToAllPayments(i) = (paymentDates(i) - valuationDate)/360;
end
timeCountFloat = timeToAllPayments(timeToAllPayments>0);
timeForAllFloatingPayment = paymentDates(timeToAllPayments>0);
timeCountFloat = timeCountFloat(1);
timeForNextFloatingPayment = timeForAllFloatingPayment(1);


if (length(paymentDates) == length(timeForAllFloatingPayment))
    timeForLastFixing = 0;
else
    timeForLastFixing = paymentDates(timeToAllPayments<=0);
    timeForLastFixing = timeForLastFixing(end);
end
    

end

%------------- END CODE -------------------------------------------------

function [timeCountFix timeForFixPayments numberOfFixPayments] = calculateTimeCountFix(valuationDate, spotDate, paymentDates)
% Calculates dates and times for fixed leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] -  containing dates for all fixed payments
%
% Outputs
%   timeCountFix - proper time count for all fixed payments
%   timeForFixPayments - the dates for all future payments 
%   numberOfFixPayments - total number of fixed payments

%------------- BEGIN CODE -----------------------------------------------

numberOfPayments = length(paymentDates);

for i = 1:numberOfPayments
    testTime(i) = paymentDates(i)-valuationDate;
end

timeForFixPayments = paymentDates(testTime>0);
numberOfFixPayments=length(timeForFixPayments);

dateVectorValuation = datevec(valuationDate);
if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
end

for j=1:numberOfFixPayments
    dateVectorFix = datevec(timeForFixPayments(j));
    if (j == 1)
        dateVectorValuation = datevec(spotDate);
    else
        dateVectorValuation = datevec(timeForFixPayments(j-1));
    end
    if dateVectorFix(3) == 31
        dateVectorFix(3) = 30;
    end
    if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
    end
    timeCountFix(j) = 1/360*(360*(dateVectorFix(1)-dateVectorValuation(1))+30*(dateVectorFix(2)-dateVectorValuation(2))+(dateVectorFix(3)-dateVectorValuation(3)));
end

end

%------------- END CODE ----------------------------------------------

function discountRate = findDiscountRate(valuationNumber, forwardMatrix, spotDate, valuationDate, timeT)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber - the number of the row corresponding to the date that
%   is valued
%   discountMatrix
%   valuationDate
%   issueDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------

if (valuationDate < spotDate)
    startValue = spotDate-valuationDate;
else
    startValue = 1;
end

coloumnNumber = timeT - valuationDate;

    if (coloumnNumber > 3653)
        coloumnNumber = 3653;
    end
    discountRate = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber))/365;
end

%------------- END CODE -------------------------------------------------
