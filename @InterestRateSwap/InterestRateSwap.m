classdef InterestRateSwap < InterestRateInstrument
    %INTERESTRATESWAP subclass of InterestRateIntstrument. Contains
    %information about an interest rate swap (fixed for IBOR)
    %   Properties:
    %   ricCode [string] = the contracts RIC-code, inherited from superclass
    %   issueDate [double] = the issue date of the contract, inherited from superclass
    %   yieldFix [double] = Yield of the fixed leg on issue date
    %   terminationDate [double] = Termination date of the contract
    %   fixDates [vector, double] = dates for payments on the fixed leg
    %   floatDates [vector, double] = dates for payment of the floating date
    %   maturityDate [double] = date for last payment on the floating leg,
    %   the instrument will not change in value after this date
    
    properties
        fixDates; % vector with dates to fixed pay 
        floatDates; % vector with dates to recieve floating payment
    end
    
    methods
        function IRS = InterestRateSwap(rc,id,yi)
            
            
            if strcmp(rc(end-1),'M')
                tempTimeToTermination = str2double(rc(8:end-2));
            elseif strcmp(rc(end-1),'Y')
                tempTimeToTermination = 12*str2double(rc(8:end-2));
            else
                error('Undefined time unit for contract length.')
            end            
                       
            [fixedPeriodLength,floatingPeriodLength] = getInterestRateSwapConvention(rc);
            
            fixN = floor(tempTimeToTermination/fixedPeriodLength);
            floatN = floor(tempTimeToTermination/floatingPeriodLength);

            tempFixDates = zeros(fixN,1);
            tempFloatDates = zeros(floatN,1);
            
            % Pretty messy for loops. If the total length of the contract isn't
            % dividible by the period length of payments, the FIRST payment
            % period will be the non standard one.
            for j = 0:fixN-1
                tempFixDates(fixN-j) = ceilToBusinessDay(addtodate(id,tempTimeToTermination-j*fixedPeriodLength,'month'));
            end

            for j = 0:floatN-1
                tempFloatDates(floatN-j) = ceilToBusinessDay(addtodate(id,tempTimeToTermination-j*floatingPeriodLength,'month'));
            end
            IRS@InterestRateInstrument(rc,id,tempFloatDates(end),tempFloatDates(end),yi);
            IRS.fixDates = tempFixDates;
            IRS.floatDates = tempFloatDates;
        end
        
% % % % %             %Calculates gradient of an IRS on a specific day
% % % % %         function gradientVector = calculateGradient(obj, riskFactorEigenMatrix, evaluationDate, businessDayList, spotMatrix)
% % % % %             numberOfPayments = length(obj.floatDates);
% % % % %             numberOfRiskFactors = size(riskFactorEigenMatrix,2);
% % % % %             c = ones(numberOfPayments,1);
% % % % % 
% % % % %             lengthToSpotDate = max(0,obj.spotDate-evaluationDate);
% % % % %             startDate = max(obj.spotDate, evaluationDate);
% % % % % 
% % % % %             %Sets r(0)T_0=0 if the evauationDate is beyond or equal to spotDate,
% % % % %             %otherwise r(T_0)T_0
% % % % %             if (lengthToSpotDate == 0)
% % % % %               interestRateTZero = 0; %r(T_0)T_0
% % % % %               eigenVectorTZero = zeros(1,numberOfRiskFactors);
% % % % %             else
% % % % %               interestRateTZero = extractDateFromRateMatrix(evaluationDate, lengthToSpotDate, businessDayList, spotMatrix);
% % % % %               eigenVectorTZero = riskFactorEigenMatrix(lengthToSpotDate, :);
% % % % %             end
% % % % % 
% % % % %             gradientVector = zeros(numberOfRiskFactors,1);
% % % % %             for k=1:numberOfPayments
% % % % %               if(obj.floatDates(k)>startDate)
% % % % %                 lengthK = calculateNumberOfBusinessDayBetweenDates(obj.floatDates(k), startDate, businessDayList);
% % % % % 
% % % % %                 gradientVector = gradientVector-(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*c(k)*exp(interestRateTZero*lengthToSpotDate-extractDateFromRateMatrix(evaluationDate, lengthK, businessDayList, spotMatrix)*lengthK);
% % % % %               end
% % % % %             end
% % % % %        end
    end
    
end

