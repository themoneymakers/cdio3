function [valueAtRisk expectedShortfall gNorm hNorm] = runPortfolioOneDayFX(portfolio, date)

%>> [valueAtRisk]=runPortfolioOneDay(PortfolioFRAIRSW, 730418)
    tic
    contractArray = getActiveContractForPortfolio(portfolio.instrumentArray, date);
    
    riskFactorUSD = evalin('base', 'riskFactorUSD');
    spotMatrixUSD = evalin('base', 'spotRateMatrixUSD');
    riskFactorSEK = evalin('base', 'riskFactorSEK');
    spotMatrixSEK = evalin('base', 'spotRateMatrixSEK');
    riskFactorEUR = evalin('base', 'riskFactorEUR');
    spotMatrixEUR = evalin('base', 'spotRateMatrixEUR');
    businessDayListCombined = evalin('base', 'businessDayListCombined');
    meanVector = evalin('base', 'meanVector');
    covarianceMatrix = evalin('base', 'covarianceMatrix');
    
    valuationRow = find(businessDayListCombined==date);
    
    numberRiskFactor = size(riskFactorUSD,2);
    
    %Riskfactor -> EURUSD, EUR, SEKUSD, SEK, USDEUR, USDSEK, USd
    
    cumulativeGradient = zeros(numberRiskFactor*3+4,1);
    cumulativeHessian = zeros(numberRiskFactor*3+4);
    
    for i=1:length(contractArray)
        if(strcmp(contractArray{i}.foreignCurrency,'EUR'))
            [gradientUSD, hessianUSD] = calculateGradientHessianFx(contractArray{i}, [ones(3652,1) riskFactorUSD],businessDayListCombined(valuationRow, 2), date, spotMatrixUSD);
            [gradientEUR, hessianEUR] = calculateGradientHessianFx(contractArray{i}, [ones(3652,1) riskFactorEUR],businessDayListCombined(valuationRow, 3), date, spotMatrixEUR);
            gradient=[gradientEUR; zeros(numberRiskFactor+1,1); gradientUSD(1); 0; gradientUSD(2:end)];
            hessian= blkdiag(hessianEUR,zeros(numberRiskFactor+1), hessianUSD(1,1),0,hessianUSD(2:end, 2:end));
        else
            [gradientUSD, hessianUSD] = calculateGradientHessianFx(contractArray{i}, [ones(3652,1) riskFactorUSD],businessDayListCombined(valuationRow, 2), date, spotMatrixUSD);
            [gradientSEK, hessianSEK] = calculateGradientHessianFx(contractArray{i}, [ones(3652,1) riskFactorSEK],businessDayListCombined(valuationRow, 4), date, spotMatrixSEK);
            
            if numel(gradientUSD)==0
                disp('break');
                gradientUSD=zeros(numberRiskFactor+1,1);
                gradientSEK=zeros(numberRiskFactor+1,1);
            end
            gradient=[zeros(numberRiskFactor+1,1);gradientSEK;   0;gradientUSD(1); gradientUSD(2:end)];
            hessian= blkdiag(zeros(numberRiskFactor+1),hessianSEK, 0,hessianUSD);
        end
        
      %Checks if contract is shorted.  
      if contractArray{i}.short
        gradient = -gradient;
        hessian = -hessian;
      end
       cumulativeGradient = cumulativeGradient + gradient;
       cumulativeHessian = cumulativeHessian + hessian;
       
    end
    
    disp(datestr(date,'dd,mmm,yyyyDddd'));
    if nargout < 2
        valueAtRisk = simulateGeneralMethodDistribution(cumulativeGradient, cumulativeHessian, meanVector, covarianceMatrix, 10000,date);
    elseif nargout >= 2
        [valueAtRisk expectedShortfall] = simulateGeneralMethodDistribution(cumulativeGradient, cumulativeHessian, meanVector, covarianceMatrix, 10000, date);
    end
    
    gNorm = norm(cumulativeGradient);
    hNorm = norm(cumulativeHessian);
    
    
    
%     % Calculate index to get te corresponding portfolio value to this date:
%     valueIndex = date-portfolio.startDate+1;
%     portValueVector=portfolio.priceVector;
%     portValue = portValueVector(valueIndex);
%     % divide to get relative VaR
%     valueAtRisk = valueAtRisk/portValue;
%     % OBS finns problem med att anv�nda ett relativt VAR-m�tt p� saker som
%     % till�ter kan anta ett negativt v�rde (eg h�r). Till skillnad fr�n
%     % till exempel aktier d�r inte v�r portf�lj bara f�r att den antar ett
%     % negativt v�rde.
    




    toc
    
end