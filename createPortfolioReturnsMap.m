function [ returnsVector ] = createPortfolioReturnsMap( portfolio, bizList )
%CREATEPORTFOLIORETURNSMAP For a portfolio with values on every date,
%returns a containers.Map with dates as keys and returns as contents
%   Detailed explanation goes here
%   Input: <Portfolio> portfolio: a portfolio
%          <double array>bizList: a businessdaylist
%
%   by: Pontus

rawPrices = portfolio.priceVector;

portStartDate = portfolio.startDate;
portEndDate = portfolio.endDate;

returnsVector = [];

for portIter = portStartDate + 1 : portEndDate
    if find(bizList == portIter)
        returnsVector = [returnsVector; rawPrices(portIter - portStartDate + 1) - rawPrices(portIter - portStartDate)];
    end    
end



end

