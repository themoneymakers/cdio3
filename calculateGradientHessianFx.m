function [gradientVector, hessianMatrix] = calculateGradientHessianFx(elementFx, riskFactorEigenMatrix, valuationNumber, valuationDate, spotMatrix)
%valuationNumber rad i spotratematrix
%riskFactorEigenMatrix - slimEigenMatrix 


businessDayListCombined=evalin('base', 'businessDayListCombined');
maturityDate = ceilToBusinessDay(elementFx.maturityDate, businessDayListCombined(:,1));
startDate = max(elementFx.spotDate, valuationDate);
[spotRate, timeToMaturity] = findSpotRateAndTime(valuationNumber, spotMatrix, startDate, maturityDate);

%Transforms variable
riskFactorEigenMatrix(:,1)=riskFactorEigenMatrix(:,1)./timeToMaturity;
% Calculate gradientVector


startDate=ceilToBusinessDay(startDate, businessDayListCombined(:,1));


lengthK = calculateNumberOfBusinessDayBetweenDates(maturityDate, startDate, businessDayListCombined(:,1));
%lengthK = calculateNumberOfBusinessDayBetweenDatesImproved(maturityDate, startDate);

gradientVector = riskFactorEigenMatrix(lengthK,:)'*timeToMaturity*exp(spotRate*timeToMaturity);

% Calculate hessianVector

hessianMatrix = riskFactorEigenMatrix(lengthK,:)'*riskFactorEigenMatrix(lengthK,:)*timeToMaturity^2*exp(spotRate*timeToMaturity);
end

function [spotRate timeToMaturity] = findSpotRateAndTime(valuationNumber, spotRateMatrix, valuationDate, maturityDate)
% Finds spot rate and time to maturity 
%Uses the spot rate matrix

%
%------------- BEGIN CODE --------------

valuationLength = maturityDate - valuationDate;
timeToMaturity = valuationLength/365;
spotRate = spotRateMatrix(valuationNumber,valuationLength);

end

%------------- END CODE --------------