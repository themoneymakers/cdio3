classdef ForwardRateAgreement < InterestRateInstrument
%FORWARDRATEAGREEMENT subclass of InterestRateIntstrument
%   Detailed explanation goes here
%Example of call:
%ForwardRateAgreement('EUR3X6F=', now, 'EUR', 5)

  properties
    floatingRate; % floating rate to be recieved at maturity as a number of months
  end

  methods
    function FRA = ForwardRateAgreement(rc,id,yi)
        
        ricCode = strrep(rc, '=', '');
        lengthString = ricCode(4:end);
        %Fixes that a Swedish FRA don't have any ending character
        if isstrprop(lengthString(end), 'alpha')
            lengthString = ricCode(4:end-1);
        end

        if isstrprop(lengthString(2), 'alpha')
            md = ceilToBusinessDay(addtodate(id,str2double(lengthString(1)),'month'));
            tempTerminationDate = ceilToBusinessDay(addtodate(id,str2double(lengthString(3:end)),'month'));
            td = tempTerminationDate;
        elseif isstrprop(lengthString(3), 'alpha')
            md = ceilToBusinessDay(addtodate(id,str2double(lengthString(1:2)),'month')); 
            tempTerminationDate = ceilToBusinessDay(addtodate(id,str2double(lengthString(4:end)),'month'));
            td = tempTerminationDate;
        else
            error('Cannot parse RicCode for FRA correctly.');
        end
        FRA@InterestRateInstrument(rc,id,md,td,yi);
        if isstrprop(lengthString(2), 'alpha')
            FRA.floatingRate  = str2double(lengthString(3:end)) - str2double(lengthString(1));
        elseif isstrprop(lengthString(3), 'alpha')
            FRA.floatingRate  = str2double(lengthString(4:end)) - str2double(lengthString(1:2));
        else
            error('Cannot parse RicCode for FRA correctly.');
        end
        
       

    end
  end
end

