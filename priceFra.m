function priceOfFra = priceFra(elementFra, valuationNumber, forwardMatrix, valuationDate)
% Price FRA at time valuationDate
% Recieves fixed rate "yieldFRA" between two times
%
% Inputs   
%   elementIrs - elements containing information about IRS
%       elementFra.issueDate (scalar)
%       elementFra.terminationDate (scalar)
%       elementFra.maturityDate (scalar)
%       elementFra.yield (scalar)
%       elementFra.nominalValue (scalar)
%   valuationNumber [double] - the number corresponding to right date in discountMatrix
%   discountMatrix [matrix] - matrix with all discount rates
%   valuationDate [double] - the date the FRA should be valued
%
% Outputs
%   priceFRA [vector] - the price/value of each FRA contract (elementFra) at valuationDate
%

%------------- BEGIN CODE --------------

numberOfInstruments = length(elementFra);
priceOfFra = zeros(numberOfInstruments,1);

for j = 1:numberOfInstruments
% Sets values
nominalValue = elementFra{j}.nominalValue;
yield = elementFra{j}.yield;
maturityDate = elementFra{j}.maturityDate;
terminationDate = elementFra{j}.terminationDate;
issueDate = elementFra{j}.issueDate;
spotDate = elementFra{j}.spotDate;
short = elementFra{j}.short;
   
% Set proper Tc with appropriate time convention
timeCountTc = timeCount(maturityDate, terminationDate);
      
discountRates = findDiscountRate(valuationNumber, forwardMatrix, valuationDate, spotDate, maturityDate, terminationDate);
discountFactorT1 = exp(-discountRates(1));
discountFactorT2 = exp(-discountRates(2));

%1+yield*timeCountTc

if short == 0
    priceOfFra(j) = nominalValue*(1+yield*timeCountTc)*discountFactorT2-nominalValue*discountFactorT1;
elseif short == 1
    priceOfFra(j) = -(nominalValue*(1+yield*timeCountTc)*discountFactorT2-nominalValue*discountFactorT1);
end

end
end

%------------- END CODE --------------

function timeCount = timeCount(firstDate, secondDate)
% Calculated the time to payment with right dayconventions
%
% Inputs   
%   firstDate
%   secondDate
%
% Outputs
%   timeCount
%
%------------- BEGIN CODE --------------
timeCount = (secondDate-firstDate)/360;

end

%------------- END CODE --------------

function discountRate = findDiscountRate(valuationNumber, forwardMatrix, valuationDate, spotDate, maturityDate, terminationDate)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber -  the number corresponding to right date in discountMatrix
%   discountMatrix
%   valuationDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------
discountRate = zeros(2,1);

if (valuationDate < spotDate)
    startValue = spotDate-valuationDate;
else
    startValue = 1;
end

if (valuationDate ~= maturityDate)
    coloumnNumber1 = maturityDate - valuationDate;
    discountRate(1) = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber1))/(365);
else
    discountRate(1) = 0;
end

% spotMatrixUSD = evalin('base', 'spotMatrixUSD');
% spot = spotMatrixUSD(valuationNumber,coloumnNumber1)
% timeTo = (maturityDate-valuationDate)/365;
% HEJ = spot*timeTo
coloumnNumber2 = terminationDate - valuationDate;
discountRate(2) = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber2))/(365);
end

%------------- END CODE --------------

