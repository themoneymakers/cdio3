function ttm = timeToMaturity(ric)
%TIMETOMATURITY Summary of this function goes here
%   Detailed explanation goes here
    
    ricCodeWOCurrency = ric(4:end);
    
    ttm = 0;
    i=1;
    while isfinite(str2double(ricCodeWOCurrency(i)))
        ttm = 10*ttm + str2double(ricCodeWOCurrency(i));
        i = i + 1;
    end
end

