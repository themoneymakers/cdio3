function getInfoRicCode(ricCode)

%type=''
%getInfoRicCode('EURAB6E1Y=')
%getInfoRicCode('EURAB6E7Y=')
%getInfoRicCode('EURAB6E25Y=')
%getInfoRicCode('EURAB6E18M=')
%getInfoRicCode('USDAM3L1Y=')
%getInfoRicCode('USDAM3L6M=')
%getInfoRicCode('USDAM3L18M=')
%getInfoRicCode('USDAM3L13Y=')
%getInfoRicCode('SEK3F1=')
%getInfoRicCode('SEKAB3S2Y=')
%getInfoRicCode('EUR3X6F=')
%getInfoRicCode('EUR8X11F=')
%getInfoRicCode('USD12X18F=')
ricCode = strrep(ricCode, '=', '')
type = '';
numberInString = isstrprop(ricCode, 'alpha');

if numberInString(4)
  %Takes values from ricCode and displays them
  type = 'IRS'
  lengthString = ricCode(8:end)
  if lengthString(end)=='Y'
    disp(lengthString(8:end-1))
  elseif lengthString(end)=='M'
    disp(lengthString(8:end-1))
  end
else
  %Takes values from ricCode and displays them
  type = 'FRA'

  lengthString = ricCode(4:end)
  %Fixes that a Swedish FRA don't have any ending character
  if isstrprop(lengthString(end), 'alpha')
    lengthString = ricCode(4:end-1)
  end
  
  if isstrprop(lengthString(2), 'alpha')
    startMonth = lengthString(1)
    endMonth  = lengthString(3:end)
  elseif isstrprop(lengthString(3), 'alpha')
    startMonth = lengthString(1:2)
    endMonth = lengthString(4:end)
  else
    error('Cannot parse RicCode for FRA correctly.')
  end
end