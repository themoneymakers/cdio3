numVec=zeros(length(runDays),1);
matVec=zeros(length(runDays),1);
shoVec=zeros(length(runDays),1);
valVec=zeros(length(runDays),1);
retVec=zeros(length(runDays),1);

for iter=1:length(runDays)
    [tempNum,tempMat,tempShort,tempVal]=numberOfActiveContracts(PortfolioFRAIRSW,runDays(iter));
    numVec(iter)=tempNum;
    matVec(iter)=tempMat;
    shoVec(iter)=tempShort;
    valVec(iter)=tempVal;
    if iter >=2
        retVec(iter)=valVec(iter)-valVec(iter-1);
    end
    disp(iter);
end;

