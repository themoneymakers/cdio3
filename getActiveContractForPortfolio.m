function [activeContracts] = getActiveContractForPortfolio(portfolioArray, date)
activeContracts={};

for i = 1:length(portfolioArray)
    if (portfolioArray{i}.maturityDate > date) && (portfolioArray{i}.issueDate <= date)
        activeContracts{end+1,1}=portfolioArray{i};
    end
end
end