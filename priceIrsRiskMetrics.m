function [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, spotRateFloat, spotRateFix, valuationDate, dateForCashFlow)
% Present value of one float or fix payment
% Recieves floating rate and pays fixed rate
% Either spotRateFix or spotRateFloat has to be zero
%
% Inputs   
%   elementIrs - element containing information about IRS
%       elementIrs.issueDate (scalar)
%       elementIrs.fixDates (vector)
%       elementIrs.floatDates (vector)
%       elementIrs.yield (scalar)
%       elementIrs.nominalValue (scalar)
%   LIBOR3M (Container) - simple three months libor rates for all dates
%   spotRateFloat [double] - Containing the floating spot rate or 0
%   spotRateFix [double] - Containing the fix spot rate or 0
%   valuationDate [double] - the date the IRS should be valued
%   dateForCashFlow [double] - the date of the cash flow. Must be a part of
%   either spotRateFix or Float
%
% Outputs
%   priceOfIrs [vector] - the present value of different cashflows
%   time [vector] - the time to the cash flows
%
% Example:
%     [price time] = priceIrsRiskMetrics(irs,LIBOR3M,0,spotRate,valuationDate,729368)
% 
%     price =
% 
%        -0.0288
% 
% 
%     time =
% 
%         0.4986

%------------- BEGIN CODE ----------------------------------------------

nominalValue = elementIrs.nominalValue;
yieldFix = elementIrs.yield;
issueDate = elementIrs.issueDate;
fixDates = elementIrs.fixDates;
floatDates = elementIrs.floatDates;

if(valuationDate > max(fixDates(end),floatDates(end)))
    error('Valuation Date greater than Maturity Date')
end

% ----------------------------------------------------------------------
% Price of floating leg
if spotRateFloat ~= 0 && spotRateFix == 0
[timeCountFloat timeForNextFloatingPayment timeForLastFixing] = calculateTimeCountFloat(valuationDate, floatDates, dateForCashFlow);
time = (timeForNextFloatingPayment-valuationDate)/365;
discountFactorFloat= exp(-spotRateFloat*time);

if (timeForLastFixing == 0)
    timeForLastFixing = issueDate;
end

yieldFloat = LIBOR3M(timeForLastFixing);

priceOfIrs = nominalValue*(1+yieldFloat*timeCountFloat)*discountFactorFloat;

elseif spotRateFloat == 0 && spotRateFix ~= 0

% -------------------------------------------
% Price of fixed leg
[timeCountFix timeForFixPayment lastDate] = calculateTimeCountFix(valuationDate, fixDates, dateForCashFlow);
time = (timeForFixPayment-valuationDate)/365;
discountFactorFix = exp(-spotRateFix*time);

priceOfIrs = -nominalValue*yieldFix*timeCountFix*discountFactorFix;

if (lastDate == 1)
    priceOfIrs = priceOfIrs-nominalValue*discountFactorFix;
end

if ~elementIrs.short
    priceOfIrs = priceOfIrs;
elseif elementIrs.short
    priceOfIrs = -priceOfIrs;
end

else
    error('Either spotRateFix or spotRateFloat has to be zero (and the other one needs to have a value)')
end
        

end

%------------- END CODE ----------------------------------------------

function [timeCountFloat timeForNextFloatingPayment timeForLastFixing] = calculateTimeCountFloat(valuationDate, paymentDates, dateForCashFlow)
% Calculates needed date and time for floating leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] - containing dates for all floating payments
%
% Outputs
%   timeCountFloat - Proper time count for next floating payment
%   timeForNextFloatingPayment - The date of the next floating payment
%   timeForLastFixing - The date of the fixing of the floating rate 

%------------- BEGIN CODE -----------------------------------------------

for i = 1:length(paymentDates)
    timeToAllPayments(i) = (paymentDates(i) - valuationDate)/360;
end

timeCountFloat = timeToAllPayments(timeToAllPayments>0);
timeForAllFloatingPayment = paymentDates(timeToAllPayments>0);
elementSpotRateFloat = length(timeToAllPayments) - length(timeForAllFloatingPayment)+1;
timeCountFloat = timeCountFloat(1);
timeForNextFloatingPayment = timeForAllFloatingPayment(1);

if (timeForNextFloatingPayment ~= dateForCashFlow)
    error('N�gon har gjort en felkalkylering av n�sta r�rliga betalning. Dubbelkolla din kod.')
end

if (length(paymentDates) == length(timeForAllFloatingPayment))
    timeForLastFixing = 0;
else
    timeForLastFixing = paymentDates(timeToAllPayments<=0);
    timeForLastFixing = timeForLastFixing(end);
end
end

%------------- END CODE -------------------------------------------------

function [timeCountFix timeForFixPayment lastDate] = calculateTimeCountFix(valuationDate, paymentDates, dateForCashFlow)
% Calculates dates and times for fixed leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] -  containing dates for all fixed payments
%
% Outputs
%   timeCountFix - proper time count for all fixed payments
%   timeForFixPayments - the dates for all future payments 
%   numberOfFixPayments - total number of fixed payments

%------------- BEGIN CODE -----------------------------------------------

numberOfPayments = length(paymentDates);

if (dateForCashFlow < valuationDate)
    error('dateCashFlow < valuationDate')
else
	timeForFixPayment = dateForCashFlow;
end

indexFix = find(paymentDates == dateForCashFlow);

if isempty(indexFix)
    error('dateCashFlow is not a part of the floating payments')
end

if (indexFix == length(paymentDates))
    lastDate = 1;
else
    lastDate = 0;
end

if (indexFix == 1)
    dateVectorValuation = datevec(valuationDate);
else
    dateVectorValuation = datevec(paymentDates(indexFix-1));
end

dateVectorFix = datevec(timeForFixPayment);

if dateVectorFix(3) == 31
    dateVectorFix(3) = 30;
end

if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
end
    timeCountFix = 1/360*(360*(dateVectorFix(1)-dateVectorValuation(1))+30*(dateVectorFix(2)-dateVectorValuation(2))+(dateVectorFix(3)-dateVectorValuation(3)));
end
