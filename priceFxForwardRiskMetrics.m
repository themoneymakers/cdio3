function [priceOfFxForwardFor priceOfFxForwardDom] = priceFxForwardRiskMetrics(elementFxForward, spotRate, strikePrice, valuationDate)

numberOfInstruments = length(elementFxForward);
priceOfFxForward = zeros(numberOfInstruments,1);

issueDate = elementFxForward.issueDate;
maturityDate = elementFxForward.maturityDate;
spotDate = elementFxForward.spotDate;

spotFor = spotRate(1);
spotDom = spotRate(2)
    
timeK = (maturityDate - issueDate)/365;
    
discountFactorK = exp((spotDom-spotFor)*timeK)
strikePriceK = strikePrice(issueDate)*discountFactorK;
   
time = (maturityDate - valuationDate)/365;
short=elementFxForward.short;


   if short == 0
        priceOfFxForwardFor = strikePrice(valuationDate)*exp(-spotFor*time)
        priceOfFxForwardDom = -strikePriceK*exp(-spotDom*time);
   elseif short == 1
        priceOfFxForwardFor = -strikePrice(valuationDate)*exp(-spotFor*time)
        priceOfFxForwardDom = strikePriceK*exp(-spotDom*time);   
   end

end