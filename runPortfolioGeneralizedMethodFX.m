startTime = now;
datestr(startTime)
portfolioStartDate= 2750;
distanceFromPortfolioStart =0; %250;
Portfolio=PortfolioFXW;
[valueAtRiskREF volatility] = calculateReferenceValueAtRisk(Portfolio);
%Is it a FX portfolio?
FX=true;

%Lags the estimates of mean/covariance of risk factors
meanLag = 360;

endDate=4812;

%Uses combined businessDayList for FX portfolios
if FX
    runDays = businessDayListCombined(portfolioStartDate+distanceFromPortfolioStart:endDate);
else
    runDays = businessDayList(portfolioStartDate+distanceFromPortfolioStart:endDate);
end
% runDays = businessDayList((2450+1315):6216); % Crashes after 28-jan-2005 (732339 (runDays(1318)) ), so changed the run period

VAR = zeros(length(runDays),1);
ES = zeros(length(runDays),1);
%priceDifference = zeros(length(runDays), 1);
%priceDifference=priceDifferenceInPortfolio(Portfolio,businessDayListCombined(:,1), businessDayListCombined(portfolioStartDate+distanceFromPortfolioStart,1), businessDayListCombined(endDate,1));
% portfolioPrices = PortfolioFXW.priceVector;
% priceDifference=portfolioPrices(2:end)-portfolioPrices(1:end-1); % Anv�nds den alls?
% priceDifference(priceDifference==0)=[];
priceDifference = createPortfolioReturnsMap( Portfolio, runDays ); % Actually creates a vector
VAR(1:2,1)=ones(2,1);
tradVAR = zeros(length(runDays), 1);
tradVAR(1:2,1)= ones(2,1);
numberOfMisses = 0;
numberOfMissesTrad = 0;
exceedDates= [];
exceedDatesTrad= [];


meanVector = mean(dailyChange)'; % N�got problem h�r, index out of bounds n�gonstans, l�ses matriserna/datum r�tt? olika beroende p� mean lag: Metoden stannar p� endDate - lag.
covarianceMatrix = cov(dailyChange);

gradientVector=[];
hessianVector=[];

for generalIterator=1:length(runDays) % evalin-ing the iterator to simulateGen... so preferred something other than i //Pontus 
%     tic
try
     tradVAR(generalIterator, 1) = calculateVaRTraditional(Portfolio, runDays(generalIterator), runDays(generalIterator));
catch
    disp('tadVAR failed, probably index out of bounds')
end
%     toc
%     tradVAR(generalIterator, 1)
    
    [VAR(generalIterator) ES(generalIterator) gNorm hNorm]= runPortfolioOneDayFX(Portfolio, runDays(generalIterator));
    %VAR(generalIterator)= runPortfolioOneDayFX(Portfolio, runDays(generalIterator));
    VAR(generalIterator)
    %ES(generalIterator)
    if -VAR(generalIterator)>=priceDifference(generalIterator) % Ska INTE j�mf�ras mot genIt mot genIt, runDays(1) = 732800 medan priceDifference(1) �r f�rsta arbetsdagen efter 730418
      numberOfMisses=numberOfMisses+1;
      exceedDates = [exceedDates generalIterator];
    end

    if -tradVAR(generalIterator)>=priceDifference(generalIterator)
      numberOfMissesTrad=numberOfMissesTrad+1;
      exceedDatesTrad = [exceedDatesTrad generalIterator];
    end
    
    gradientVector=[gradientVector;gNorm];
    hessianVector = [hessianVector;hNorm];
    
    meanVector = mean(dailyChange(portfolioStartDate+distanceFromPortfolioStart+generalIterator:portfolioStartDate+distanceFromPortfolioStart+meanLag+generalIterator,:));
    covarianceMatrix = cov(dailyChange(portfolioStartDate+distanceFromPortfolioStart+generalIterator:portfolioStartDate+distanceFromPortfolioStart+meanLag+generalIterator,:));
    
    
    generalIterator
    
    pause(0.02)

end

VAR

endTime = now;
datestr(startTime)
datestr(endTime)

