function priceOfFxForward = priceFxForward(elementFxForward, valuationNumberDomestic, valuationNumberForeign, businessDayListDomestic, businessDayListForeign, forwardMatrixDomestic, forwardMatrixForeign, strikePrice, valuationDate)

numberOfInstruments = length(elementFxForward);
priceOfFxForward = zeros(numberOfInstruments,1);

for j = 1:numberOfInstruments
   issueDate = elementFxForward{j}.issueDate;
   maturityDate = elementFxForward{j}.maturityDate;
   spotDate = elementFxForward{j}.spotDate;
   short = elementFxForward{j}.short;
   
   issueNumberDomestic = find(businessDayListDomestic == issueDate);
   issueNumberForeign = find(businessDayListForeign == issueDate);

   k = 1;
   while isempty(issueNumberForeign)
       issueNumberForeign = find(businessDayListForeign == (issueDate+k));
       k = k + 1;
   end
   
   discountRateDomesticK = findDiscountRate(issueNumberDomestic, spotDate, forwardMatrixDomestic,issueDate, maturityDate);
   discountRateForeignK = findDiscountRate(issueNumberForeign, spotDate, forwardMatrixForeign, issueDate, maturityDate);
   strikePriceK = strikePrice(issueDate)*exp(discountRateDomesticK-discountRateForeignK);
   discountRateDomestic = findDiscountRate(valuationNumberDomestic, spotDate, forwardMatrixDomestic, valuationDate, maturityDate);
   discountRateForeign = findDiscountRate(valuationNumberForeign, spotDate, forwardMatrixForeign, valuationDate, maturityDate);
   
   if short == 0
       priceOfFxForward(j) = strikePrice(valuationDate)*exp(-discountRateForeign)-strikePriceK*exp(-discountRateDomestic);
   elseif short == 1
       priceOfFxForward(j) = -(strikePrice(valuationDate)*exp(-discountRateForeign)-strikePriceK*exp(-discountRateDomestic));
   end
   
end

end

function discountRate = findDiscountRate(valuationNumber, spotDate, forwardMatrix, valuationDate, maturityDate)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber - the number of the row corresponding to the date that
%   is valued
%   discountMatrix
%   valuationDate
%   issueDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------

if (valuationDate < spotDate)
    startValue = spotDate-valuationDate;
else
    startValue = 1;
end
    coloumnNumber = maturityDate - valuationDate;


if (coloumnNumber >3653)
    coloumnNumber = 3653;
end

    discountRate = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber))/(365);
end