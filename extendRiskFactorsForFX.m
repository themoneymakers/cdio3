function [dailyChangeFXUSD, dailyChangeFXForeign] = extendRiskFactorsForFX(foreign, numberOfRiskFactors)

numberOfRiskFactors=6;

% contractDatabase=strcat('cur',foreign,'USD');
% contractDatabase=evalin('base', contractDatabase);
% 
% businessDayList=evalin('base','businessDayListCombined');
% forwardMatrixUSD=evalin('base','forwardMatrixUSD');
% forwardMatrixForeign=evalin('base',strcat('forwardMatrix',foreign));

%Convert forwardMatrix to combinedBusinessDayList
forwardMatrixUSD = forwardMatrixUSD(:,1:3653);

forwardMatrixUSDSlim = zeros(length(businessDayListCombined), 3653);
forwardMatrixEURSlim = zeros(length(businessDayListCombined), 3653);
forwardMatrixSEKSlim = zeros(length(businessDayListCombined), 3653);
for iter=1:length(businessDayListCombined)
   forwardMatrixUSDSlim(iter,:) = forwardMatrixUSD(businessDayListCombined(iter,2),:);
   forwardMatrixEURSlim(iter,:) = forwardMatrixEUR(businessDayListCombined(iter,3),:);
   forwardMatrixSEKSlim(iter,:) = forwardMatrixSEK(businessDayListCombined(iter,4),:);
end

[riskFactorUSD, e, change, dailyChangeUSD ]=determineRiskFactors(numberOfRiskFactors,forwardMatrixUSDSlim);
[riskFactorEUR, e, change, dailyChangeEUR ]=determineRiskFactors(numberOfRiskFactors,forwardMatrixEURSlim);
[riskFactorSEK, e, change, dailyChangeSEK ]=determineRiskFactors(numberOfRiskFactors,forwardMatrixSEKSlim);

disp('test')

riskFactorChangeUSDSEK=zeros(length(businessDayListCombined)-1,numberOfRiskFactors+1);
riskFactorChangeUSDEUR=zeros(length(businessDayListCombined)-1,numberOfRiskFactors+1);
riskFactorChangeSEKUSD=zeros(length(businessDayListCombined)-1,numberOfRiskFactors+1);
riskFactorChangeEURUSD=zeros(length(businessDayListCombined)-1,numberOfRiskFactors+1);

%for i=1:3652 forwardDiff(:,i)=forwardMatrixUSD(:,i+1)-forwardMatrixUSD(:,i); end;
for i=2:length(businessDayListCombined)
    riskFactorChangeUSDSEK(i,:)=[log(curSEKUSD(businessDayListCombined(i)))-log(curSEKUSD(businessDayListCombined(i-1))) dailyChangeUSD(i,:)];
    riskFactorChangeSEKUSD(i,:)=[log(1/curSEKUSD(businessDayListCombined(i)))-log(1/curSEKUSD(businessDayListCombined(i-1))) dailyChangeSEK(i,:)];
    riskFactorChangeUSDEUR(i,:)=[log(curEURUSD(businessDayListCombined(i)))-log(curEURUSD(businessDayListCombined(i-1))) dailyChangeUSD(i,:)];
    riskFactorChangeEURUSD(i,:)=[log(1/curEURUSD(businessDayListCombined(i)))-log(1/curEURUSD(businessDayListCombined(i-1))) dailyChangeEUR(i,:)];
end


end

