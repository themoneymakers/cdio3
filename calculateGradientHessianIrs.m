function [gradientVector, hessianMatrix] = calculateGradientHessianIrs(interestRateInstrument, riskFactorEigenMatrix, evaluationDate, businessDayList, spotMatrix)
%>> [a,b]=calculateGradientIrs(testContract, riskFactorEigenMatrix, 731920, businessDayList, spotMatrixUSD)

businessDayImprovedList=evalin('base', 'businessDayImprovedList');

%Select the number of payments if it is FRA/IRS
if isa(interestRateInstrument,'ForwardRateAgreement')
    paymentDayList = [interestRateInstrument.maturityDate, interestRateInstrument.terminationDate ];
else    
    %Calculate floatDate
    floatDates = interestRateInstrument.floatDates;
    floatDates = floatDates(floatDates>evaluationDate);
    
    %Calculate fixDate
    fixDates = interestRateInstrument.fixDates;
    fixDates = fixDates(fixDates>evaluationDate);
    
    %List of payment days
    paymentDayList = [floatDates(1) fixDates']';
end

%Changes sign on gradient/Hessian if the instrument is shorted
if interestRateInstrument.short == false
  multiplier = -1;
else
  multiplier = 1;
end

numberOfRiskFactors = size(riskFactorEigenMatrix,2);
c = calculateCVector(interestRateInstrument, evaluationDate);

%Checks if C and paymentList are of same length
if(length(c)~=length(paymentDayList))
    error('c and paymentDayList are not of the same dimension')
end

lengthToSpotDate = max(0,interestRateInstrument.spotDate-evaluationDate);
startDate = max(interestRateInstrument.spotDate, evaluationDate);

%Sets r(0)T_0=0 if the evauationDate is beyond or equal to spotDate,
%otherwise r(T_0)T_0
if (lengthToSpotDate == 0)
  interestRateTZero = 0; %r(T_0)T_0
  eigenVectorTZero = zeros(1,numberOfRiskFactors);
else
  interestRateTZero = spotMatrix(businessDayImprovedList(evaluationDate-726834, 2), lengthToSpotDate);
  eigenVectorTZero = riskFactorEigenMatrix(lengthToSpotDate, :);
end

gradientVector = zeros(numberOfRiskFactors,1);
hessianMatrix = zeros(numberOfRiskFactors);

for k=1:length(paymentDayList)
  if(paymentDayList(k)>startDate)
    lengthK = calculateNumberOfBusinessDayBetweenDatesImproved(paymentDayList(k), startDate, businessDayList);
    
    matrixValue=spotMatrix(businessDayImprovedList(evaluationDate-726834, 2), lengthK);
    
    gradientVector = gradientVector-multiplier*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*c(k)*exp(interestRateTZero*lengthToSpotDate-matrixValue*lengthK);
    hessianMatrix = hessianMatrix-multiplier*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)*c(k)*exp(interestRateTZero*lengthToSpotDate-matrixValue*lengthK); 
        
  end
end

end