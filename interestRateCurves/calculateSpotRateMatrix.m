function [spotRateMatrix] = calculateSpotRateMatrix(forwardMatrix)
% calculateSpotRateMatrix - Calculates a spot rate matrix out of a forward
% matrix
%
% Inputs:
%  forwardMatrix [matrix]  

% Outputs:
%    spotRateMatrix [matrix] - The spotRate for the whole rate curve
%
% Help functions:
%    calculateDiscountMatrix
%
% Other m-files required: calculateDiscountMatrix
% MAT-files required: none
% Other files required: none
%
% Author: Johan Hyd�n
% November 2014; Last revision: 17-11-2014

T=1./(1:size(forwardMatrix, 2))';
discountMatrix = calculateDiscountMatrix(forwardMatrix);
spotRateMatrix=discountMatrix*diag(T);

end

