function [valueAtRisk volatility pastReturns priceValue] = calculateReferenceValueAtRisk(Portfolio)

priceValue = Portfolio.priceVector;
pastReturns = calculatePastReturnsGeometric(priceValue);
volatility = calculateVolatilityAll(pastReturns);

valueAtRisk = volatility * 1.96;

end

function pastReturns = calculatePastReturnsGeometric(valueOfInstrument)
% calculatePastReturns - Calculates the log-returns
% Uses past values of instruments to calculate past returns
%
% Inputs:
%    valueOfInstrument [matrix] - values of instrument over a time period,
%    each coloumn is an asset, and each row is a time (from newest
%    to oldest)
%
% Outputs:
%    pastReturns [matrix] - The log-returns of each asset at each time
%
% Other m-files required: calculatePriceFra, calculatePriceIrs (...etc)
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------

numberOfData = length(valueOfInstrument); 
pastReturns = zeros(numberOfData-1,1);

for i = 1:length(valueOfInstrument)-1
    pastReturns(i) = (valueOfInstrument(i+1)-valueOfInstrument(i))/valueOfInstrument(i);
end

end

%------------- END OF CODE --------------

function [pastVolatilities] = calculateVolatilityAll(pastReturns)
% calculateVolatility - Calculates past volatilities
% Uses past returns to calculate past volatilites using EWMA
%
% Inputs:
%    pastReturns [matrix] - past returns of instruments over a time period.
%    Each coloumn is an asset, and each row is a time (from newest
%    to oldest)
%
% Outputs:
%    pastVolatilities [matrix] - The past volatilites of each asset at each time
%
% Other m-files required: calculatePastReturns
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------
LAMBDA = 0.94;
numberOfData = length(pastReturns);
pastVolatilities=zeros(numberOfData,1);
meanPastReturn = mean(pastReturns);

% ----------------------------------------
% Calculate the starting value for EWMA

pastVolatilities(1) = sum((pastReturns - meanPastReturn).^2)/(numberOfData-1);

% -----------------------------------------
% Calculate matrix of volatilites with EWMA

for i = 1:numberOfData-1
    pastVolatilities(i+1) = LAMBDA*pastVolatilities(i)+(1-LAMBDA)*pastReturns(i)^2;
end


%-----------------------------------------
% Converting the variance to volatility
% Converting daily volatility to yearly
pastVolatilities = sqrt(pastVolatilities);

end

%------------- END OF CODE --------------