function binomialTest(VaR,price)
% binomialTest - Measures the number of infraction
% 
% Inputs:
%   VaR
%    
%
% Outputs:
%    
%
%
%    
%    
%Other Matlab-files required
%generalizedVaREs
%calculateVaRTraditional
%
% Author: Evelina Stenvall
% December 2014; Last revision: 01-12-2014

%------------- BEGIN CODE --------------
lowLimit = 0.05;
highLimit = 0.01;
nrToHigh=0;


for i=1:length(VaR)
    if VaR(i)>price(i)
        nrToHigh=nrToHigh+1;      
    end
end

[PHAT_LOW, PCI_LOW] =binofit(nrToHigh,length(VaR),lowLimit)
[PHAT_HIGH, PCI_HIGH] =binofit(nrToHigh,length(VaR),highLimit)


%[PHAT, PCI] = binofit(X,N,ALPHA); x nr of sample, prob success,0.01/0.05
%phat = ML estimate of porb of success
%pci = confidence interval
end