function [differenceInPrice] = priceDifferenceInPortfolio(portfolio,businessDayList, startDate, endDate)

if nargin < 3
    startDate = portfolio.startDate;
    endDate = portfolio.endDate;
end

numberOfBusinessDays=calculateNumberOfBusinessDayBetweenDates(portfolio.startDate,portfolio.endDate, businessDayList)+1;
priceBusinessDays=zeros(numberOfBusinessDays,1);

ind=1;
i=1;
for currentDate=startDate:endDate
    if find(currentDate==businessDayList)
       priceBusinessDays(ind)=portfolio.priceVector(i);
       ind=ind+1;
    end
    i=i+1;
end

differenceInPrice=zeros(numberOfBusinessDays-1,1);
for i=1:numberOfBusinessDays-1
differenceInPrice(i)=priceBusinessDays(i+1)-priceBusinessDays(i);
end

end