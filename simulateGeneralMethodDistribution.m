function [valueAtRisk expectedShortfall] = simulateGeneralMethodDistribution(gVector, matrixHessian, meanVector, covarianceMatrix, numberOfIterations, day)
% functionName - One line description of what the function performs (H1 line)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Examples: 
% >> simulateGeneralMethodDistribution(ones(20,1), ones(20)*10, zeros(20,1), 10*eye(20), 1000000)
%
% ans =
%
% 10 
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: emil och pontus
% September 2014; Last revision: 14-Sep-2014

%------------- BEGIN CODE --------------
% % Regular
% result = zeros(numberOfIterations,1);
% randomMatrix = mvnrnd(meanVector, covarianceMatrix, numberOfIterations)';
% for i=1:numberOfIterations
%     randomMatrixObservation = randomMatrix(:,i);
%     result(i,1) = gVector'*randomMatrixObservation+1/2.*randomMatrixObservation'*matrixHessian*randomMatrixObservation;
% end
% 
% valueAtRisk = prctile(result, 0.05);


% Hupercube
result = zeros(numberOfIterations,1);
normalHyperCube = lhsnorm(meanVector, covarianceMatrix, numberOfIterations)';
preMatrixHessian=1/2.*matrixHessian;

for i=1:numberOfIterations
    randomMatrixObservation = normalHyperCube(:,i);
    result(i,1) = gVector'*randomMatrixObservation+randomMatrixObservation'*preMatrixHessian*randomMatrixObservation;
end

if nargout<2
    valueAtRisk = -prctile(result, 5); % Var on 5-percent level
elseif nargout==2 % Only do this if needed (prtctile may be faster to get Var, but we need to sort to calculate ES)
    sortedResult=sort(result); %Sort in ascending order
    limit = ceil(0.05*numberOfIterations)+1;
    valueAtRisk = -sortedResult(limit);
    expectedShortfall = -mean(sortedResult(1:limit));
    subplot(2,3,1)
    histfit(result,100);
    hold on
    valueAtRiskREF = evalin('base', 'valueAtRiskREF');
    redX = [-valueAtRisk,-valueAtRisk];
    redY = [0;250];
    VARPlot=plot(redX,redY,'r', 'DisplayName','VAR - Generalized');
    greenX = [-expectedShortfall, -expectedShortfall];
    greenY = [0;250];
    ESPlot=plot(greenX,greenY,'g', 'DisplayName','ES - Generalized');  
    priceDifference=evalin('base', 'priceDifference');
    priceDiffDay = evalin('base','generalIterator');
    priceDiff = priceDifference(priceDiffDay);
    yellowX = [priceDiff, priceDiff];
    yellowY = [0;250];
    priceDiffPlot=plot(yellowX,yellowY,'y', 'DisplayName','Price difference');
    skew=skewness(result);
    histPlotTitle=strcat(datestr(day),sprintf('\nskewedness: '),num2str(skew));
    title(histPlotTitle);
    
    tradVAR = evalin('base', 'tradVAR');
    
    blackX = [-tradVAR(priceDiffDay) , -tradVAR(priceDiffDay) ];
    blackY = [0;250];
    tradVARPlot=plot(blackX, blackY, 'm', 'DisplayName','VAR - Traditional');
    
    magnetaX = [-valueAtRiskREF(priceDiffDay), -valueAtRiskREF(priceDiffDay)];
    magnetaY = [0;250];
    %VARrefPlot=plot(magnetaX, magnetaY, 'm', 'DisplayName','VAR - Reference');
    
    dailyChange = evalin('base', 'dailyChange');
    dailyChangeAnswer = dailyChange(priceDiffDay+1,:)';
    cyanX = [gVector'*dailyChangeAnswer+dailyChangeAnswer'*preMatrixHessian*dailyChangeAnswer, gVector'*dailyChangeAnswer+dailyChangeAnswer'*preMatrixHessian*dailyChangeAnswer];
    cyanY = [0;250];
    priceDiffOnPlot=plot(cyanX, cyanY, 'c', 'DisplayName','Control based on RF - Generalized');
    
    legend([VARPlot, ESPlot, priceDiffPlot, priceDiffOnPlot, tradVARPlot])%, VARrefPlot])
    hold off
    subplot(2,3,2)
    qqplot(result)
    [hLillie,pLillie] = lillietest(result);
    [hJb,pJb] = jbtest(result);
    xlabel(sprintf('Normality test(P-values):\nLilliefors: p=%.2f\nJarque-Bera: p=%.2f', pLillie, pJb));
    
    subplot(2,3,3)
    VAR = evalin('base', 'VAR');
    plot(VAR(VAR~=0));
    title('plot over generalized-VAR')
    xlabel(sprintf('Andel missar generaliserad: %.5f', evalin('base', 'numberOfMisses/generalIterator')));
    if -valueAtRisk > priceDifference(priceDiffDay)
        set(subplot(2,3,3),'Color','Red')
    end
    
    subplot(2,3,4)
    disp(priceDiffDay-2449);
    try
%         ewmaplot(priceDifference(1:priceDiffDay-2449));
        plot(priceDifference(1:priceDiffDay));
        if priceDifference(priceDiffDay)==0
            % Förmodligen helgdag
            set(subplot(2,3,4),'Color','Red')
            disp('HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEELG!!!!11111');
        end
        title('plot over priceChange')
    catch
        disp('caught error');
    end
    
    subplot(2,3,5)
    try
%         ewmaplot(valueAtRiskREF(1:priceDiffDay-2449));
        plot(tradVAR(1:priceDiffDay));
        title('plot over traditional VAR')
        xlabel(sprintf('Andel missar traditional: %.5f', evalin('base', 'numberOfMissesTrad/generalIterator')));
    catch
        disp('caught error');
    end
    
    
    
    subplot(2,3,6)
    try
        gradientVector = evalin('base', 'gradientVector');
        hessianVector = evalin('base', 'hessianVector');
        
        gradientVector = [gradientVector ; norm(gVector)];
        hessianVector = [hessianVector ; norm(matrixHessian)];
        
        generalIterator = evalin('base', 'generalIterator');
        
        plot(1:generalIterator , gradientVector , 1:generalIterator , hessianVector);
        title('norm of gradient (blue) and hessian (green)')
        
    catch
        disp('error in the norms')
    end
    
    
    
    
    pause(0.05)
end

disp('###################################################################')


end


%------------- END OF CODE --------------

