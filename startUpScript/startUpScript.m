%Startup script
tic
clc;
clear;
load('BlomvallTransformationUSD5.mat');
load('businessDayList.mat');
load('LIBOR3M.mat')
load('BlomvallTransformationSEK1.mat')
load('BlomvallTransformationEUR1.mat')
load('currencySpot databases.mat')
load('Portfolios\PortfolioFRAIRSW.mat')
% load('Portfolios\PortfolioFXW.mat') 
% load('Portfolios\PortfolioFRAIRSD.mat') 
% load('Portfolios\PortfolioFXD.mat') 
% 
% businessDayListCombined=combinedBusinessDayList(businessDayListUSD, businessDayListSEK, businessDayListEUR);
% 
spotMatrixUSD = calculateSpotRateMatrix(forwardMatrixUSD);
[ riskFactorUSD, e, change, dailyChange ]=determineRiskFactors(6,forwardMatrixUSD);
riskFactorUSD = calculateSpotRateMatrix(riskFactorUSD')';
meanVector = mean(dailyChange(2:end,:));
covarianceMatrix = cov(dailyChange(2:end,:));
% 
discountMatrixUSD=calculateDiscountMatrix(forwardMatrixUSD(:,1:3653));
% discountMatrixSEK=calculateDiscountMatrix(forwardMatrixSEK);
discountMatrixEUR=calculateDiscountMatrix(forwardMatrixEUR);
% 
spotRateMatrixUSD=calculateSpotRateMatrix(forwardMatrixUSD(:,1:3653));
% spotRateMatrixSEK=calculateSpotRateMatrix(forwardMatrixSEK);
spotRateMatrixEUR=calculateSpotRateMatrix(forwardMatrixEUR);
% 
[valueAtRiskREF volatility] = calculateReferenceValueAtRisk(PortfolioFRAIRSW);
% 
% [ riskFactorUSD, e, change, dailyChange ]=determineRiskFactors(6,extendRiskFactorsForFX('EUR'));

toc