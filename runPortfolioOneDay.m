function [valueAtRisk expectedShortfall] = runPortfolioOneDay(portfolio, date)

%>> [valueAtRisk]=runPortfolioOneDay(PortfolioFRAIRSW, 730418)
    tic
    contractArray = getActiveContractForPortfolio(portfolio.instrumentArray, date);
    
    riskFactorUSD = evalin('base', 'riskFactorUSD');
    spotMatrixUSD = evalin('base', 'spotMatrixUSD');
    businessDayList = evalin('base', 'businessDayList');
    meanVector = evalin('base', 'meanVector');
    covarianceMatrix = evalin('base', 'covarianceMatrix');
    
    numberRiskFactor = size(riskFactorUSD,2);
    
    cumulativeGradient = zeros(numberRiskFactor,1);
    cumulativeHessian = zeros(numberRiskFactor);
    
    for i=1:length(contractArray)
 %       if isa(contractArray{i})
       [gradient, hessian] = calculateGradientHessianIrs(contractArray{i}, riskFactorUSD, date, businessDayList, spotMatrixUSD);
       
       cumulativeGradient = cumulativeGradient + gradient;
       cumulativeHessian = cumulativeHessian + hessian;
       
    end
    
    disp(datestr(date));
    if nargout < 2
        valueAtRisk = simulateGeneralMethodDistribution(cumulativeGradient, cumulativeHessian, meanVector, covarianceMatrix, 10000,date);
    elseif nargout == 2
        [valueAtRisk expectedShortfall] = simulateGeneralMethodDistribution(cumulativeGradient, cumulativeHessian, meanVector, covarianceMatrix, 10000, date);
    end
    
%     % Calculate index to get te corresponding portfolio value to this date:
%     valueIndex = date-portfolio.startDate+1;
%     portValueVector=portfolio.priceVector;
%     portValue = portValueVector(valueIndex);
%     % divide to get relative VaR
%     valueAtRisk = valueAtRisk/portValue;
%     % OBS finns problem med att anv�nda ett relativt VAR-m�tt p� saker som
%     % till�ter kan anta ett negativt v�rde (eg h�r). Till skillnad fr�n
%     % till exempel aktier d�r inte v�r portf�lj bara f�r att den antar ett
%     % negativt v�rde.
    toc
    
end