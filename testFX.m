% %Test FXF gradient/hessian
% 
% numberOfRiskFactor = 6;
% testContract=PortfolioFXW.instrumentArray{1};
% 
% date=730420;
% row=2451;
% 
% callString = strcat('cur',testContract.foreignCurrency,testContract.domesticCurrency,'(',num2str(date),')');
% 
% exchangeRate = evalin('base', callString); %Number of USD you get from one EUR
% 
% 
% 
% foreignRiskFactor=...%[1 EUR to USD]
%     riskFactorUSD;
% domesticRiskFactor=riskFactorUSD;
% 
% [g, H] = calculateGradientHessianFx(testContract, foreignRiskFactor, 2451, 730420, spotRateMatrixUSD)

foreign='EUR'

contractDatabase=strcat('cur',foreign,'USD');
contractDatabase=evalin('base', contractDatabase);

businessDayList=evalin('base','businessDayList');
forwardMatrixUSD=evalin('base','forwardMatrixUSD');
forwardMatrixForeign=evalin('base',strcat('forwardMatrix',foreign));

[riskFactorUSD, e, change, dailyChangeUSD ]=determineRiskFactors(numberOfRiskFactors,forwardMatrixUSD);
[riskFactorForeign, e, change, dailyChangeForeign ]=determineRiskFactors(numberOfRiskFactors,forwardMatrixForeign);

spotRateUSD=zeros(length(businessDayList)-1,1);
spotRateForeign=zeros(length(businessDayList)-1,1);


%for i=1:3652 forwardDiff(:,i)=forwardMatrixUSD(:,i+1)-forwardMatrixUSD(:,i); end;
for i=1:length(businessDayList)
    spotExchangeRate=contractDatabase(businessDayList(i));
    spotRateUSD(i)=spotExchangeRate;
    spotRateForeign(i)=1/spotExchangeRate;
end

dailyChangeFXUSD=[spotRateUSD(2:end)-spotRateUSD(1:end-1) dailyChangeUSD(2:end,:)]

dailyChangeFXForeign=[spotRateForeign(2:end)-spotRateUSD(1:end-1) dailyChange(2:end,:)]