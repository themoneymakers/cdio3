classdef InterestRateInstrument < FinancialInstrument
    %INSTRUMENT Base class for the financial instruments
    %   Base class, might be useful because there is only small difference
    %   between the different instruments. All instruments inheirits
    %   fundamental properties and methods from this class.
    
    properties
        % member data

        %I'm not sure if these should be under IRS or IRS/FRA?
        yield;
        terminationDate;
        
    end
    
    methods
        function IRI = InterestRateInstrument(rc,id,md,td,yi) % constructor
            IRI@FinancialInstrument(rc,id,md)
            IRI.yield = yi/100;
            IRI.terminationDate = ceilToBusinessDay(td);

        end
    end
end

%currency = ricCodeString(1:3); %First three letters of ricCode is the currency