function [shortArray, midArray, longArray] = splitContractByLength(contractArray)
% importExcelFile - The file creates Instrument object from a sheet in Excel file
%
% Inputs:
%   contractArray - Array of contracts
%
% Outputs:
%   shortArray - Array of short contracts (<= 1 year)
%   midArray - Array of mid contracts (<= 5 year)
%   longArray - Array of long contracts (> 5 year)
%
% Examples: 
% >> [shortArray, midArray, longArray] =
% splitContractByLength(getInstrumentArray(usdDatabase('20100111')))
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: 10-November-2014

%------------- BEGIN CODE --------------

shortArray = {};
shortIter = 1;
midArray = {};
midIter = 1;
longArray = {};
longIter = 1;
numberOfContract = length(contractArray);

for contractIterator = 1 : numberOfContract
    if (contractArray{contractIterator}.maturityDate)-(contractArray{contractIterator}.issueDate) <= 0
        error('Negative or zero day contracts does not exist.')
    elseif (contractArray{contractIterator}.maturityDate)-(contractArray{contractIterator}.issueDate) <= 366
        shortArray{shortIter} = contractArray{contractIterator};
        shortIter = shortIter + 1;
    elseif (contractArray{contractIterator}.maturityDate)-(contractArray{contractIterator}.issueDate) <= 1827
        midArray{midIter} = contractArray{contractIterator};
        midIter = midIter + 1;
    else
        longArray{longIter} = contractArray{contractIterator};
        longIter = longIter + 1;
    end
end
  

