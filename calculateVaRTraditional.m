
% calculateVaRTraditional - Calculates the VaR with traditional method
% 
%
% Inputs:
%    forwardMatrix [matrix] 
%    Portfolio
%
% Outputs:
%    VaRTraditional [vector] - The VaR using traditional method
%
% Help functions:
%    pastReturn Matrix
%    
%Other Matlab-files required
%calculateRiskMetricsCashFlows

%
% Author: Evelina Stenvall
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------

function traditionalVaR = calculateVaRTraditional(Portfolio, valuationDateStart, valuationDateEnd)%ska portf�ljen ocks� in?
if nargin <2
    STARTDATE = Portfolio.startDate;
    ENDDATE = Portfolio.endDate;
else
    if (Portfolio.startDate <= valuationDateStart && Portfolio.endDate >= valuationDateEnd && valuationDateStart <= valuationDateEnd)
        STARTDATE = valuationDateStart;
        ENDDATE = valuationDateEnd;
    else
        error('Valuation date not in portfolio');
    end
end

businessDayListUSD=evalin('base','businessDayList');
LIBOR3M =evalin('base','LIBOR3M'); 
strikePriceExchangeEUR = evalin('base','curEURUSD');
strikePriceExchangeSEK = evalin('base','curSEKUSD');

discountMatrixUSD=evalin('base','discountMatrixUSD');
% discountMatrixSEK=evalin('base','discountMatrixSEK'); % needs some
% if/else here to check if interest or currency portfolio
% discountMatrixEUR=evalin('base','discountMatrixEUR');

spotRateMatrixUSD=evalin('base','spotRateMatrixUSD');
% spotRateMatrixSEK=evalin('base','spotRateMatrixSEK');% needs some
% if/else here to check if interest or currency portfolio
% spotRateMatrixEUR=evalin('base','spotRateMatrixEUR');

startNumber = find(businessDayListUSD == ceilToBusinessDayImproved(STARTDATE));
endNumber = find(businessDayListUSD == ceilToBusinessDayImproved(ENDDATE));   
    


traditionalVaR=zeros(endNumber-startNumber+1,1);    
CashFlow=0;
k=0;

for i=startNumber:endNumber
    STARTDATE=businessDayListUSD(i);
    activeContracts = getActiveContractForPortfolio(Portfolio.instrumentArray,STARTDATE);
    for j=1:length(activeContracts);% ska det vara activeContracts-1 s� att den sista i calcRMCF �r sigma?
        
        if isa(activeContracts{j}, 'ForeignExchangeForward')

            valuationNumberSEK=i;
            valuationNumberEUR=i;
            CF = calculateRiskMetricsCashFlows(activeContracts{j}, i, STARTDATE, discountMatrixUSD, spotRateMatrixUSD, LIBOR3M,... 
                discountMatrixSEK,discountMatrixEUR, valuationNumberSEK ,valuationNumberEUR,spotRateMatrixSEK,spotRateMatrixEUR, strikePriceExchangeEUR, strikePriceExchangeSEK);
        else
            CF = calculateRiskMetricsCashFlows(activeContracts{j},i,STARTDATE,discountMatrixUSD,spotRateMatrixUSD,LIBOR3M);
        end

        if isreal(CF)
            CashFlow = CashFlow + CF;
            CF=0;
        else
            error('imagin�rt kassafl�de!');
        end
    end
    Weights = CashFlow./sum(CashFlow);    
    pastReturnMatrix = calculatePastReturnMatrix(i,STARTDATE,discountMatrixUSD);
    correlationMatrix=calculateCorrelationMatrix(pastReturnMatrix); %ska nog �ndras?
    sigma = calculateVolRmVertics(i,discountMatrixUSD,STARTDATE);
    VaRi= Weights.*sigma*1.96;
    VaR = sqrt(VaRi*correlationMatrix*VaRi');
    traditionalVaR(k+1,1)=VaR;
    k=k+1;
end
end


function volRmVertics=calculateVolRmVertics(valuationNumber,discountMatrix,valuationDate)
riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
for i=1:11
    presentValueBond(:,i) = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate,riskMetricsMaturities(i));
    
end

pastReturnsBond = calculatePastReturns(presentValueBond);
pastVolatility = calculateVolatility(pastReturnsBond);
volRmVertics=pastVolatility;

end


%----------------------Past Returns-------------------------
%hj�lpfunktion f�r att ber�kna pastReturnMatrix
function pastReturnMatrix = calculatePastReturnMatrix(valuationNumber,valuationDate,discountMatrix)

 pastReturnsOneMonth = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),1/12));
 pastReturnsThreeMonths = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),3/12));
 pastReturnsSixMonths = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),6/12));
 pastReturnsOneYear = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),1));
 pastReturnsTwoYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),2));
 pastReturnsThreeYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),3));
 pastReturnsFourYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),4));
 pastReturnsFiveYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),5));
 pastReturnsSevenYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),7));
 pastReturnsNineYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),9));
 pastReturnsTenYears = calculatePastReturns(valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),10));
 
 pastReturnMatrix=[pastReturnsOneMonth pastReturnsThreeMonths pastReturnsSixMonths pastReturnsOneYear pastReturnsTwoYears pastReturnsThreeYears pastReturnsFourYears pastReturnsFiveYears pastReturnsSevenYears pastReturnsNineYears pastReturnsTenYears];
 
end
