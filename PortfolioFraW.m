% Portfolio containing FRA and IRS. Updated every two week since 1999
startDate = datenum(2002,10,25);
Fra=usdFraDataBase(startDate);
Irs=usdIrsDataBase(startDate);
contractsFra={Fra{15};Fra{20}};
activeContractsFra = contractsFra;
contractsIrs={Irs{1};Irs{6}};
activeContractsIrs = contractsIrs;
endDate = startDate + 250
%endDate = datenum(2014,10,30);
SHORT_DIST_FRAIRS=0.36;
MID_DIST_FRAIRS=0.42;
LONG_DIST_FRAIRS=0.27; 
k=2;
l=2;
n=-1;
sumOfPriceFra = zeros(endDate-startDate+1,1);
sumOfPriceIrs = zeros(endDate-startDate+1,1);
cash = 0;
cash_vector = 0;

    for i=startDate:endDate
      DAAG =  i-startDate+1
      valuationNumber = find(businessDayList == i);
      n=n+1;
      if n==14
     %------------------------------FRA------------------------------         
       if isContractsIssued(i,usdFraDataBase)
        [instr_short,instr_mid,instr_long]=splitContractByLength(usdFraDataBase(i));%ska �ndras

        if not(isempty(instr_short))&& not(isempty(valuationNumber))
            n=length(instr_short);
            tmp=randi(n);
            for j=1:tmp
            m=length(instr_short);
            temp=randi(m);
            k=k+1;
            instr_short{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsFra{k,1}= instr_short{temp};
            activeContractsFra{end+1} = contractsFra{k,1};
            temp=0;
            end
        tmp=0;
        end
      end

     
%-------------------------------IRS------------------------------
     n=0;
      end
      if(not(isempty(valuationNumber)))
    % FRA, plocka ut aktiva och matured kontrakt
        cash_temp = 0;
        [activeContractsFra maturedContracts] = getActiveContract(activeContractsFra,i);
        length(activeContractsFra)
        cash_temp = priceFra(maturedContracts, valuationNumber,forwardMatrixUSD,i);
        cash = cash + sum(cash_temp);
        
        cash_vector(i-startDate+1,1) = cash;

        %V�rdera kontrakt 
        priceOfFra = priceFra(activeContractsFra, valuationNumber,forwardMatrixUSD,i)
        sumOfPriceFra(i-startDate+1,1)=sum(priceOfFra);
      else
        sumOfPriceFra(i-startDate+1,1) = sumOfPriceFra(i-startDate,1);
        cash_vector(i-startDate+1,1) = cash_vector(i-startDate,1);
    end
 end
    sumOfPortfolio = sumOfPriceFra + cash_vector;
    contractsArray=[contractsFra;contractsIrs];
    PortfolioFRAIRSW=Portfolio(contractsArray,sumOfPortfolio,datenum(1999,10,25),datenum(2014,10,30));