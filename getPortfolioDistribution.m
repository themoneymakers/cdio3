function[short, mid, long]=getPortfolioDistribution(contracts,today)
l=length(contracts);
NR_SHORT=0;
NR_MID=0;
NR_LONG=0;

for i=1:l
    matDate=contracts{i}.maturityDate;
    if matDate-today <=366 && matDate - today >0
        NR_SHORT=NR_SHORT+1;
    elseif matDate-today <= 1827 && matDate -today >0
        NR_MID = NR_MID +1;
    elseif matDate-today > 1827
        NR_LONG=NR_LONG+1;
    end
end
short=NR_SHORT/(NR_SHORT+NR_MID+NR_LONG);
mid=NR_MID/(NR_SHORT+NR_MID+NR_LONG);
long=NR_LONG/(NR_SHORT+NR_MID+NR_LONG);