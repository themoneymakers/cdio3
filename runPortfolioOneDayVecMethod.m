function [valueAtRisk expectedShortfall] = runPortfolioOneDayVecMethod(portfolio, date)

%>> [valueAtRisk]=runPortfolioOneDay(PortfolioFRAIRSW, 730418)
    tic
    contractArray = getActiveContractForPortfolio(portfolio.instrumentArray, date);
    
    meanVector = evalin('base', 'meanVector');
    covarianceMatrix = evalin('base', 'covarianceMatrix'); 
    numberOfIterations=10000;
    
    result = zeros(numberOfIterations,1);
    normalHyperCube = lhsnorm(meanVector, covarianceMatrix, numberOfIterations);

    result = calculatePortfolioValue(contractArray, date, normalHyperCube);
    
    disp(datestr(date));

    if nargout < 2
        valueAtRisk = prctile(result, 5);
    end
    
%     % Calculate index to get te corresponding portfolio value to this date:
%     valueIndex = date-portfolio.startDate+1;
%     portValueVector=portfolio.priceVector;
%     portValue = portValueVector(valueIndex);
%     % divide to get relative VaR
%     valueAtRisk = valueAtRisk/portValue;
%     % OBS finns problem med att anv�nda ett relativt VAR-m�tt p� saker som
%     % till�ter kan anta ett negativt v�rde (eg h�r). Till skillnad fr�n
%     % till exempel aktier d�r inte v�r portf�lj bara f�r att den antar ett
%     % negativt v�rde.
    toc
    
end