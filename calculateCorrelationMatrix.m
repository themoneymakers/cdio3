function correlationMatrix = calculateCorrelationMatrix(pastReturns)
% calculateCorrelationMatrix - Calculates past covariance
% Uses past returns of two assets to calculate covariance
%
% Inputs:
%    pastReturns [matrix] - past returns of instruments over a time period.
%    Each coloumn is an asset, and each row is a time (from newest to oldest).
%    dateVector [vector] - vector with dates corresponding to pastReturns
%    valuationTime [scalar] - the date for which the correlation matrix
%    should be calculated
%
% Outputs:
%    correlationMatrix [matrix] - The correlation matrix for time
%    "valuationTime"
%
% Help functions:
%    createCovarianceMatrix
%    calculatePastCovariance
%
% Other m-files required: calculatePastReturns
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------

[numberOfData numberOfAssets] = size(pastReturns);
correlationMatrix = zeros(numberOfAssets);

% -----------------------------------------
% Pick out the date that should be valued

% --------------------------------
% Calculate Volatility
pastVolatilityTemp = calculateVolatility(pastReturns);
pastVolatility = pastVolatilityTemp(1,:);

% ---------------------------------  
% Create covariance matrix
covarianceMatrix = createCovarianceMatrix(pastReturns);

% ---------------------------------  
% Create correlation matrix

for i = 1: numberOfAssets
    for j = 1:numberOfAssets
        correlationMatrix(i,j)= covarianceMatrix(i,j)/(pastVolatility(i)*pastVolatility(j));
        if ne(i,j)
            correlationMatrix(j,i) = correlationMatrix(i,j);
        end
    end
end
end

%------------- END OF CODE --------------

%%------------ HELP FUNCTIONS -----------------------------------------


function covarianceMatrix = createCovarianceMatrix(pastReturns)
% createCovarianceMatrix - Creates covariance matrix
% Uses past returns and valuation number to create covarince matrix for a
% given date
%
% Inputs:
%    pastReturns [matrix] - past returns of instruments over a time period.
%    Each coloumn is an asset, and each row is a time (from newest
%    to oldest)
%    valuationNumber [scalar] - the date the covariance matrix should be valued for transformed to a number
%
% Outputs:
%    covarianceMatrix [matrix] - Covariance matrix 
%
% 

%------------- BEGIN CODE --------------

[numberOfData numberOfAssets] = size(pastReturns);

covarianceMatrix = zeros(numberOfAssets);

for i = 1:numberOfAssets
    for j = 1:numberOfAssets
        pastCovarianceTemp = calculatePastCovariance(pastReturns(:,i),pastReturns(:,j));
        covarianceMatrix(i,j) = pastCovarianceTemp(1);
        if ne(i,j)
            covarianceMatrix(j,i) = pastCovarianceTemp(1);
        end
    end
end

end

%------------- END OF CODE --------------

function pastCovariance = calculatePastCovariance(pastReturnsI, pastReturnsJ)
% calculatePastCovariance - Calculates past covariance
% Uses past returns of two assets to calculate covariance
%
% Inputs:
%    pastReturnsI [vector] - past returns of an instrument over a time period.
%    Each row is a time (from newest to oldest)
%    pastReturnsJ [vector] - past returns of an instrument over a time period.
%    Each row is a time (from newest to oldest)
%
% Outputs:
%    pastCovariance [vector] - The past covariance at each time
%
% Help functions:
%    calculatePastCovariance
%

%------------- BEGIN CODE --------------

LAMBDA = 0.94;
numberOfData = length(pastReturnsI);
pastCovariance = zeros(numberOfData,1);

% --------------------------------
% Calculate the starting value
meanPastReturnI = mean(pastReturnsI);
meanPastReturnJ = mean(pastReturnsJ);

for j = 1:numberOfData
    sumOfReturns(j) = (pastReturnsI(j) - meanPastReturnI)*(pastReturnsJ(j)-meanPastReturnJ);
end

pastCovariance(numberOfData) = 1/(numberOfData-1)*sum(sumOfReturns);

%----------------------------------
% Calculate past covariance with EWMA

for i = numberOfData-1:-1:1
    pastCovariance(i) = LAMBDA*pastCovariance(i+1)+(1-LAMBDA)*pastReturnsI(i+1)*pastReturnsJ(i+1);
end

%-------------------------------
% Converting from daily to yearly

pastCovariance = pastCovariance*252;

end

%------------- END OF CODE --------------
