% H�mta data fr�n transf och data collection

%I nul�get mappas kassafl�den f�r en enhet av ett visst instrument, v�rde m�ste g�ngras med antal
%tillg�ngar fr�n portf�lj

function mappingCfRiskMetrics=rm_gammal(element, valuationDate, issueDate)

riskMetricsMaturities=365*[0 1/12 3/12 1/2 1 2 3 4 5 7 9 10 15 20 30];

for i=1:14
    if (timeToMaturity>riskMetricsMaturities(15))
        timeToMaturityRM=[0 riskMetricsMaturities(15)];
    elseif (timeToMaturity==riskMetricsMaturities(i))
        timeToMaturityRM=[0 riskMetricsMaturities(i)];
    elseif(riskMetricsMaturities(i)<timeToMaturity<riskMetricsMaturities(i+1))
        timeToMaturityRM=[riskMetricsMaturities(i) riskMetricsMaturities(i+1)];
    end
end


%----------step 1: interpolate zero-yield from RiskMetrics cash flows-------------

%interpolatedYield=zeros(3,1); tre yields som ska skickas till FRA

yieldsRM=[1 2]; %Get from data transformation
interpolatedYield=interpolate(timeToMaturityAcual,timeToMaturityRM,yieldsRM);   %Calculates interpolated zero-yield

%interpolate(0,timeToMaturityY1,timeToMaturityY2, yield1, yield2);   %Calculates interpolated zero-yield


%----------step 2: calculate PV w/ riskmetrics yields-------------

% V�LDIGT OKLART

%m�ste g�ra en for loop s� det redan h�r blir flera v�rden som kan anv�ndas
%till calc.pastreturns... 

%if (strcmp('element','elementFxForward'))                   %m�ste fixa hur man tar reda p� typ
%    value=valuateFxForward(spotPriceCurrency,riskFreeRateDomestic,riskFreeRateForeign, timeToMaturity, maturity);
%elseif (strcmp('element','elementFra'))
%    value=priceIrs(1,2,3,4,5);                                  
%elseif (strcmp('element','elementIrs'))
%    value=priceFra(element, spotRate, valuationDate);
%end

%----------step 3: interpolate standarddeviation for acual cash flow-------------

%dateVector=[startdate:valuationDate];
%spotPriceCurrency(i)=[SpotRatesRx(dateVector(i))];                         %h�mta fr�n data collection
%riskFreeRateDomestic(i)=[riskFreeRateDomestic(dateVector(i))];             %h�mta fr�n data  transformation
%riskFreeRateForeign(i)=[riskFreeRateForeign(dateVector(i))];               %h�mta fr�n data  transformation
%timeToMaturity
%maturity
i=0;

% Fel, m�ste g�ra interpolation f�r varje v�rde, g�r hj�lpfunktion

if (strcmp('element','elementFxForward')) 
    pastValuesInstrument=zeros(maturity-timeToMaturity,1); 
    while (timeToMaturity+i =< maturity)                                                          
        pastValuesInstrument(i,1)=valuateFxForward(spotPriceCurrency(i),riskFreeRateDomestic(i),riskFreeRateForeign(i), timeToMaturity+i, maturity);
        i=i+1;
    end

else
    %f�r IRS, FRA

end


pastReturnsCF1=calculatePastReturns(pastValuesInstrument);         %Valuate f�r X antal datum? 
pastReturnsCF2=calculatePastReturns(pastValuesInstrument);

stDeviationRM=[0 0]
stDeviationRM(1)=calculateVolatility(pastReturnsCF1);           %h�mta fr�n EWMA 
stDeviationRM(2)=calculateVolatility(pastReturnsCF2);

%correlation_CF1CF2=calculateCorrelationMatrix(pastReturns, dateVector,valuationTime);        %h�mta fr�n alexandra, hur m�nga datum kr�vs? 

interpolatedStDeviation=interpolate(timeToMaturityAcual,timeToMaturityRM,stDeviationRM);   

%----------step 4: calcualate allocation (alpha and (1-alpha) for RiskMetrics CF-------------

a=stDeviationCF1^2+stDeviationCF2^2-2*correlation*stDeviationCF1*stDeviationCF2;
b=2*correlation*stDeviationCF1*stDeviationCF2-2*stDeviationCF2^2;
c=stDeviationCF1^2-stDeviationCF2^2

alphaSub=(-b-sqrt(b^2-4*a*b))/(2*a);
alphaAdd=(-b+sqrt(b^2-4*a*b))/(2*a);

if (0=<alphaSub=<1)
    alpha=alphaSub;
else
    alpha=alphaAdd;
end

%----------step 5: calcualate RiskMetrics CF-------------

riskMetricsCF1=alpha*acualCF;
riskMetricsCF2=(1-alpha)*acualCF;

end

function interpolate(timeToMaturityAcualCF,timeToMaturityRM, variableRM)

if (timeToMaturityRM(1)==0)
    interpolatedValue=variableRM(2);        
else
    interpolatedValue=variableRM(1)+(variableRM(2)-variableRM(1))*(timeToMaturityAcualCF-timeToMaturityRM(1))/(timeToMaturityRM(2)-timeToMaturityRM(2));
end