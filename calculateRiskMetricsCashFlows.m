
%OBS OBS Medlemsvariabeln "yieldFix" i InterestRateSwap heter nu bara "yield" 
%maturityDate i InterestRateSwap defineras nu som den n�st sista betalningen p� det r�rliga benet.


%OBS! Tag h�nsyn till blankning
%   if element.short==1
%       blanka, v�nd p� kassafl�de
% inargument fiskmetrixcashflow: [curEURUSD, curSEKUSD]

function riskMetricsCashFlow=calculateRiskMetricsCashFlows(element, valuationNumberUSD, valuationDate, discountMatrixUSD, spotRateMatrixUSD, LIBOR3M,...
    discountMatrixSEK,discountMatrixEUR, valuationNumberSEK ,valuationNumberEUR,spotRateMatrixSEK,spotRateMatrixEUR,strikePriceExchangeEUR, strikePriceExchangeSEK)

if nargin < 7

    discountMatrixSEK=0;
    discountMatrixEUR=0;
    valuationNumberSEK=0;
    valuationNumberEUR=0;
    spotRateMatrixSEK=0;
    spotRateMatrixEUR=0;
    strikePriceExchange=0;
    
end
    
%--------Anv�nd som inargument------------------
%riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
%cashFlowsIrs=[0 0 0 0 0 0 0 0 0 0 0];
%zeroYields=calculateZeroYields(valuationNumber,spotRateMatrix, riskMetricsMaturities);
riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
cashFlowsIrs=[0 0 0 0 0 0 0 0 0 0 0];
zeroYields=calculateZeroYields(valuationNumberUSD,spotRateMatrixUSD, riskMetricsMaturities);

[pastReturnsBond, volRmVertics]=calculateVolRmVertics(valuationNumberUSD,discountMatrixUSD,valuationDate,riskMetricsMaturities);
if isa(element, 'ForwardRateAgreement')
    riskMetricsCashFlow=calculateRiskMetricsFra(element,discountMatrixUSD, valuationNumberUSD, valuationDate, spotRateMatrixUSD);
elseif isa(element, 'InterestRateSwap')
    riskMetricsCashFlow=calculateRiskMetricsIrs(element,LIBOR3M,discountMatrixUSD, valuationNumberUSD, valuationDate, spotRateMatrixUSD);
elseif isa(element, 'ForeignExchangeForward')
    riskMetricsCashFlow=calculateRiskMetricsFx(element,discountMatrixUSD,discountMatrixSEK,discountMatrixEUR, valuationNumberUSD, ...
        valuationNumberSEK ,valuationNumberEUR, valuationDate, spotRateMatrixUSD,spotRateMatrixSEK,spotRateMatrixEUR,strikePriceExchangeEUR, strikePriceExchangeSEK);
    
else
    error('wrong type of element input');
end

%riskMetricsCashFlow=calculateRiskMetricsFra(element,discountMatrix, valuationNumber, valuationDate, spotRateMatrix)
%riskMetricsCashFlow=calculateRiskMetricsIrs(element,LIBOR3M,discountMatrix, valuationNumber, valuationDate, spotRateMatrix);
%riskMetricsCashFlow=sum(riskMetricsCashFlows);
end

function cashFlowsIrs=calculateRiskMetricsIrs(elementIrs,LIBOR3M,discountMatrix, valuationNumber, valuationDate, spotRateMatrix)
%riskMetricsCashFlowIrs
% Price IRS at valuationDate
% Recieves floating rate and pays fixed rate
%
% Inputs   
%   elementIrs - element containing information about IRS
%       elementIrs.issueDate (scalar)
%       elementIrs.fixDates (vector)
%       elementIrs.floatDates (vector)
%       elementIrs.yieldFix (scalar)
%       elementIrs.nominalValue (scalar)
%       [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, spotRateFloat, spotRateFix, valuationDate)
riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
cashFlowsIrs=[0 0 0 0 0 0 0 0 0 0 0];
zeroYields=calculateZeroYields(valuationNumber,spotRateMatrix, riskMetricsMaturities);
[pastReturnsBond, volRmVertics]=calculateVolRmVertics(valuationNumber,discountMatrix,valuationDate,riskMetricsMaturities);
%cashFlowsIrs(find(riskMetricsMaturities==date))=cashflow
%valuationNumber(1) = find(businessDayList==valuationDate(1));

fixedDate=elementIrs.fixDates;
floatDates=elementIrs.floatDates;   
%Calculates interpolated yields for fix dates:
y=find(fixedDate>valuationDate);
r=y(1);
for iFixDates=r:size(fixedDate) %size(fixedDate)-1
    daysToCashFlowFix(iFixDates)=fixedDate(iFixDates)-valuationDate;
    timeToMaturityRmFix=calcTimeToMaturityRm(daysToCashFlowFix(iFixDates));
    numberCf=length(timeToMaturityRmFix);
   
    if numberCf==1
        k=find(12*riskMetricsMaturities==timeToMaturityRmFix);
        [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, 0, zeroYields(k), valuationDate, fixedDate(iFixDates));
        presentValueIrs=priceOfIrs;
%[presentValueIrs, time]= priceIrsRiskMetrics(elementIrs, LIBOR3M, 0,
%zeroYieldFix, valuationDate, timeToMaturityRmFix/12);  GES NU I �R
        cashFlowsIrs(k)=cashFlowsIrs(k)+presentValueIrs;
    else
        k=find(12*riskMetricsMaturities==timeToMaturityRmFix(1));
        j=find(12*riskMetricsMaturities==timeToMaturityRmFix(2));

        zeroYieldsFix(1)=zeroYields(k);
        zeroYieldsFix(2)=zeroYields(j);
        interpolatedZeroYieldFix=interpolate(daysToCashFlowFix(iFixDates),ceil(365*timeToMaturityRmFix/12), zeroYieldsFix); %OBS! Kolla oxh j�mf�r interpolated volatility ci

        pastVolatility=[volRmVertics(k), volRmVertics(j)];
        correlationVertics1_2Fix=calculateCorrelation(pastReturnsBond(:,k), pastReturnsBond(:,j));
        interpolatedVolatilityFix=interpolate(daysToCashFlowFix(iFixDates),ceil(365*timeToMaturityRmFix/12), pastVolatility);
        
        [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, 0, interpolatedZeroYieldFix, valuationDate, fixedDate(iFixDates));
        presentValueIrs=priceOfIrs;

        alphaFix=calculateAlphaRiskMetrics(pastVolatility, interpolatedVolatilityFix, correlationVertics1_2Fix);
        
        [cashFlowsIrs1, cashFlowsIrs2]=calculateRiskMetricsCf(alphaFix, daysToCashFlowFix(iFixDates), timeToMaturityRmFix, presentValueIrs);
        cashFlowsIrs(k)=cashFlowsIrs(k)+cashFlowsIrs1;
        cashFlowsIrs(j)=cashFlowsIrs(j)+cashFlowsIrs2;
    end
end
x=find(floatDates>valuationDate);
l=x(1);

daysToCashFlowFloat=floatDates(l)-valuationDate;

timeToMaturityRmFloat=calcTimeToMaturityRm(daysToCashFlowFloat);
numberCf=length(timeToMaturityRmFloat);

if numberCf==1
    k=find(12*riskMetricsMaturities==timeToMaturityRmFloat);
    [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M,zeroYields(k), 0, valuationDate, floatDates(l));
    presentValueIrsFloat=priceOfIrs;
%[presentValueIrsFloat, time]= priceIrsRiskMetrics(elementIrs, LIBOR3M, 0,
%zeroYieldFix, valuationDate, timeToMaturityRmFix/12);  GES NU I �R
    cashFlowsIrs(k)=cashFlowsIrs(k)+presentValueIrsFloat;
else
    k=find(12*riskMetricsMaturities==timeToMaturityRmFloat(1));
    j=find(12*riskMetricsMaturities==timeToMaturityRmFloat(2));
       
    zeroYieldsFloat=[zeroYields(k), zeroYields(j)];
    interpolatedZeroYieldFloat=interpolate(daysToCashFlowFloat,ceil(365*timeToMaturityRmFloat/12), zeroYieldsFloat);

    pastVolatility=[volRmVertics(k) volRmVertics(j)];
    correlationVertics1_2Float=calculateCorrelation(pastReturnsBond(:,k), pastReturnsBond(:,j));
    interpolatedVolatilityFloat=interpolate(daysToCashFlowFloat,ceil(365*timeToMaturityRmFloat/12), pastVolatility);
    
    [priceOfIrsFloat time] = priceIrsRiskMetrics(elementIrs, LIBOR3M,interpolatedZeroYieldFloat, 0, valuationDate, floatDates(l));
    presentValueIrsFloat=priceOfIrsFloat;
    
    alphaFloat=calculateAlphaRiskMetrics(pastVolatility, interpolatedVolatilityFloat, correlationVertics1_2Float);

    [cashFlowsIrsFloat1, cashFlowsIrsFloat2]=calculateRiskMetricsCf(alphaFloat,daysToCashFlowFloat,timeToMaturityRmFloat, presentValueIrsFloat);
    cashFlowsIrs(k)=cashFlowsIrs(k)+cashFlowsIrsFloat1;
    cashFlowsIrs(j)=cashFlowsIrs(j)+cashFlowsIrsFloat2;
   

end

end


function riskMetricsCashFlowFra=calculateRiskMetricsFra(element,discountMatrix, valuationNumber, valuationDate, spotRateMatrix)

riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
riskMetricsCashFlowFra=[0 0 0 0 0 0 0 0 0 0 0];
zeroYields=calculateZeroYields(valuationNumber,spotRateMatrix, riskMetricsMaturities);
[pastReturnsBond, volRmVertics]=calculateVolRmVertics(valuationNumber,discountMatrix,valuationDate,riskMetricsMaturities);

daysToCashFlow(1)=element.maturityDate-valuationDate;
daysToCashFlow(2)=element.terminationDate-valuationDate;

timeToMaturityRm=calcTimeToMaturityRm(daysToCashFlow(1));
timeToTerminationRm=calcTimeToMaturityRm(daysToCashFlow(2));

numberCfMaturity=length(timeToMaturityRm);
numberCfTermination=length(timeToTerminationRm);

 if numberCfMaturity==1
     
     kMaturity=find(12*riskMetricsMaturities==timeToMaturityRm);
     
     if numberCfTermination==1
         kTermination=find(12*riskMetricsMaturities==timeToTerminationRm);
         presentValueFRA=priceFraRiskMetrics(element, [zeroYields(kMaturity), zeroYields(kTermination)], valuationDate);
     else
         kTermination=find(12*riskMetricsMaturities==timeToTerminationRm(1));
         jTermination=find(12*riskMetricsMaturities==timeToTerminationRm(2));
         
         interpolatedZeroYieldTermination=interpolate(daysToCashFlow(2),ceil(365*timeToTerminationRm/12), [zeroYields(kTermination), zeroYields(jTermination)]);
         presentValueFRA=priceFraRiskMetrics(element, [zeroYields(kMaturity), interpolatedZeroYieldTermination], valuationDate);
     end
     
     riskMetricsCashFlowFra(kMaturity)=riskMetricsCashFlowFra(kMaturity)+presentValueFRA;

 else
     
     kMaturity=find(12*riskMetricsMaturities==timeToMaturityRm(1));
     jMaturity=find(12*riskMetricsMaturities==timeToMaturityRm(2));
     
     interpolatedZeroYieldMaturity=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm/12), [zeroYields(kMaturity), zeroYields(jMaturity)]);    
     
     if numberCfTermination==1
         kTermination=find(12*riskMetricsMaturities==timeToTerminationRm);
         presentValueFRA=priceFraRiskMetrics(element, [interpolatedZeroYieldMaturity, zeroYields(kTermination)], valuationDate);
     else
         kTermination=find(12*riskMetricsMaturities==timeToTerminationRm(1));
         jTermination=find(12*riskMetricsMaturities==timeToTerminationRm(2));
         
         interpolatedZeroYieldTermination=interpolate(daysToCashFlow(2),ceil(365*timeToTerminationRm/12), [zeroYields(kTermination), zeroYields(jTermination)]);
         presentValueFRA=priceFraRiskMetrics(element, [interpolatedZeroYieldMaturity, interpolatedZeroYieldTermination], valuationDate);
     end
     
     pastVolatility=[volRmVertics(kMaturity), volRmVertics(jMaturity)];
     correlationVertics1_2=calculateCorrelation(pastReturnsBond(kMaturity,:), pastReturnsBond(jMaturity,:));
     interpolatedVolatility=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm/12), pastVolatility);
     alphaFra=calculateAlphaRiskMetrics(pastVolatility, interpolatedVolatility, correlationVertics1_2);
     
     [cashFlowsFra1, cashFlowsFra2]=calculateRiskMetricsCf(alphaFra,daysToCashFlow(1),timeToMaturityRm, presentValueFRA);
     riskMetricsCashFlowFra(kMaturity)=riskMetricsCashFlowFra(kMaturity)+cashFlowsFra1;
     riskMetricsCashFlowFra(jMaturity)=riskMetricsCashFlowFra(jMaturity)+cashFlowsFra2;

 end    

end


function riskMetricsCashFlowFx=calculateRiskMetricsFx(element,discountMatrixUSD,discountMatrixSEK,discountMatrixEUR, valuationNumberUSD,...
    valuationNumberSEK ,valuationNumberEUR, valuationDate, spotRateMatrixUSD,spotRateMatrixSEK,spotRateMatrixEUR,strikePriceExchangeEUR, strikePriceExchangeSEK)
% strikePriceExchange=[curSEKUSD curEURUSD]

riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];
riskMetricsCashFlowFx=[0 0 0 0 0 0 0 0 0 0 0];
zeroYieldsDomestic=calculateZeroYields(valuationNumberUSD,spotRateMatrixUSD, riskMetricsMaturities);
daysToCashFlow=element.maturityDate-valuationDate;
timeToMaturityRm=calcTimeToMaturityRm(daysToCashFlow);
numberCfMaturity=length(timeToMaturityRm);
[pastReturnsBondDomestic, volRmVerticsDomestic]=calculateVolRmVertics(valuationNumberUSD,discountMatrixUSD,valuationDate,riskMetricsMaturities);


 if strcmp(element.foreignCurrency, 'EUR')
    zeroYieldsForeign=calculateZeroYields(valuationNumberEUR,spotRateMatrixEUR, riskMetricsMaturities);
    strikePrice=strikePriceExchangeEUR;
    [pastReturnsBondForeign, volRmVerticsForeign]=calculateVolRmVertics(valuationNumberEUR,discountMatrixEUR,valuationDate,riskMetricsMaturities);

 elseif strcmp(element.foreignCurrency, 'SEK')
    zeroYieldsForeign=calculateZeroYields(valuationNumberSEK,spotRateMatrixSEK, riskMetricsMaturities);
    strikePrice=strikePriceExchangeSEK;
    [pastReturnsBondForeign, volRmVerticsForeign]=calculateVolRmVertics(valuationNumberSEK,discountMatrixSEK,valuationDate,riskMetricsMaturities);

 else
     error('calculateRiskMetricsCashFlows kan bara ber�kna kassafl�den f�r FX-kontrakt i SEK och EUR, n�got annat har skickats in');
 end

 if numberCfMaturity==1
     
     t=find(12*riskMetricsMaturities==timeToMaturityRm);
     spotRate=[zeroYieldsForeign(t) zeroYieldsDomestic(t)];
     presentValueFxVector=priceFxForwardRiskMetrics(element, spotRate, strikePrice, valuationDate);
     presentValueFx=sum(presentValueFxVector);
     riskMetricsCashFlowFx(t)=riskMetricsCashFlowFx(t)+presentValueFx;

 else
     
     t=find(12*riskMetricsMaturities==timeToMaturityRm(1));
     t2=find(12*riskMetricsMaturities==timeToMaturityRm(2));
     
     interpolatedZeroYieldForeign=interpolate(daysToCashFlow,ceil(365*timeToMaturityRm/12), [zeroYieldsForeign(t), zeroYieldsForeign(t2)]);    
     interpolatedZeroYieldDomestic=interpolate(daysToCashFlow,ceil(365*timeToMaturityRm/12), [zeroYieldsDomestic(t), zeroYieldsDomestic(t2)]);
     spotRate=[interpolatedZeroYieldForeign, interpolatedZeroYieldDomestic];
     
     pastVolatilityDomestic=[volRmVerticsDomestic(t), volRmVerticsDomestic(t2)];
     pastVolatilityForeign=[volRmVerticsForeign(t), volRmVerticsForeign(t2)];
     correlationVertics1_2Domestic=calculateCorrelation(pastReturnsBondDomestic(t,:), pastReturnsBondDomestic(t2,:));
     correlationVertics1_2Foreign=calculateCorrelation(pastReturnsBondForeign(t,:), pastReturnsBondForeign(t2,:));
     interpolatedVolatilityDomestic=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm/12), pastVolatilityDomestic);
     interpolatedVolatilityForeign=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm/12), pastVolatilityForeign);
     alphaDomestic=calculateAlphaRiskMetrics(pastVolatilityDomestic, interpolatedVolatilityDomestic, correlationVertics1_2Domestic);
     alphaForeign=calculateAlphaRiskMetrics(pastVolatilityForeign, interpolatedVolatilityForeign, correlationVertics1_2Foreign);
     
     [pVForeignYield pVDomesticYield] = priceFxForwardRiskMetrics(element, spotRate, strikePrice, valuationDate);
      
     [cashFlowsFx1, cashFlowsFx2]=calculateRiskMetricsCf(alphaDomestic,daysToCashFlow,timeToMaturityRm, pVDomesticYield);
     [cashFlowsFx3, cashFlowsFx4]=calculateRiskMetricsCf(alphaForeign,daysToCashFlow,timeToMaturityRm, pVForeignYield);
     
     riskMetricsCashFlowFx(t)=riskMetricsCashFlowFx(t)+cashFlowsFx1+cashFlowsFx3;
     riskMetricsCashFlowFx(t2)=riskMetricsCashFlowFx(t2)+cashFlowsFx2+cashFlowsFx4;

end
end

%----------step 4: calcualate allocation (alpha and (1-alpha) for RiskMetrics CF-------------
function alpha=calculateAlphaRiskMetrics(pastVolatility,interpolatedVolatility, pastCorrelationVetics1_2)

a=pastVolatility(1)^2+pastVolatility(2)^2-2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2);
b=2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2)-2*pastVolatility(2)^2;
c=pastVolatility(2)^2-interpolatedVolatility^2;

alphaSub=(-b-sqrt(b^2-4*a*c))/(2*a);
alphaAdd=(-b+sqrt(b^2-4*a*c))/(2*a);

if (0<=alphaSub<=1)
    alpha=alphaSub;
else
    alpha=alphaAdd;
end

end


%------------Help function: interpolate------------------------------------
%linear interpolation function, used to determaine RiskMetric's yields and volatilities---

function interpolatedValue=interpolate(timeToMaturityAcualCF,timeToMaturityRM, variableRM)
        interpolatedValue=variableRM(1)+(variableRM(2)-variableRM(1))*(timeToMaturityAcualCF-timeToMaturityRM(1))/(timeToMaturityRM(2)-timeToMaturityRM(1));
end

%---------Help function: timeToMaturityRm----------------------------------
%decides which RiskMetrics vertics the actual cash flow occur inbetween. 

function zeroYield=calculateZeroYields(valuationNumber, spotRateMatrix, timeToMaturity)

for i=1:11
zeroYield(i)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturity(i))))-1;
end

end

function [pastReturnsBond, volRmVertics]=calculateVolRmVertics(valuationNumber,discountMatrix,valuationDate,riskMetricsMaturities)

for i=1:11
    presentValueBond(:,i) = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate,riskMetricsMaturities(i));
    
end

pastReturnsBond = calculatePastReturns(presentValueBond);

pastVolatility = calculateVolatility(pastReturnsBond);
volRmVertics=pastVolatility;

end

function [cashFlows1, cashFlows2]=calculateRiskMetricsCf(alpha,daysToCashFlow,timeToMaturityRm, presentValueIrs)

timeToMaturityRm=365*timeToMaturityRm/12;

if (daysToCashFlow-timeToMaturityRm(1))<(timeToMaturityRm(2)-daysToCashFlow)
    if alpha>(1-alpha)
        cashFlows1=alpha*presentValueIrs;
        cashFlows2=(1-alpha)*presentValueIrs;
    else
        cashFlows2=alpha*presentValueIrs;
        cashFlows1=(1-alpha)*presentValueIrs;
    end
else
    if alpha<(1-alpha)
        cashFlows1=alpha*presentValueIrs;
        cashFlows2=(1-alpha)*presentValueIrs;
    else
        cashFlows2=alpha*presentValueIrs;
        cashFlows1=(1-alpha)*presentValueIrs;
    end
end

end


function timeToMaturityRm=calcTimeToMaturityRm(timeToMaturity)

riskMetricsMaturities=[1/12 3/12 1/2 1 2 3 4 5 7 9 10];

j=0;
i=1;
while j==0
    if timeToMaturity<365*riskMetricsMaturities(1)
        timeToMaturityRm=12*riskMetricsMaturities(1);
        j=1;
    elseif timeToMaturity==365*riskMetricsMaturities(i)
        timeToMaturityRm=12*riskMetricsMaturities(i);
        j=1;
    elseif timeToMaturity>365*riskMetricsMaturities(i) && timeToMaturity<365*riskMetricsMaturities(i+1)
         timeToMaturityRm=[12*riskMetricsMaturities(i) 12*riskMetricsMaturities(i+1)];
         j=1;
    elseif timeToMaturity>365*riskMetricsMaturities(11)
        timeToMaturityRm=12*riskMetricsMaturities(11);
        j=1;
    elseif i>11 && j==0
        timeToMaturity;
        error('bajs');
        break;
        j=1;
    else
        i=i+1;
    end
end

end



    
    
    
    
    
   % if(360*riskMetricsMaturities(i)<timeToMaturity && timeToMaturity<360*riskMetricsMaturities(i+1))
   %     timeToMaturityRm=[12*riskMetricsMaturities(i) 12*riskMetricsMaturities(i+1)];
   %     j=1;
   % elseif (timeToMaturity==360*riskMetricsMaturities(i) && timeToMaturity ~=0)
   %     timeToMaturityRm=[0 12*riskMetricsMaturities(i)];
   %     j=1;
   % elseif (timeToMaturity>360*riskMetricsMaturities(12))
   %     timeToMaturityRm=[0 12*riskMetricsMaturities(12)];
   %     j=1;
   % elseif (timeToMaturity==0)
   %     timeToMaturityRm=[0 12*riskMetricsMaturities(2)];
   %     j=1;
   % elseif (timeToMaturityRm(1)==0 && timeToMaturityRm(2)==0  && i<12)
   %     i=i+1;
   % else
   %     error('error in calcTimeToMaturity')
   % end

