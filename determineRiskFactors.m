function [ riskFactors, explanation, changeInForwardRate, dailyChangeInForwardRate ] = determineRiskFactors( numberOfRiskFactors, forwardMatrix )
% determineRiskFactors - determines the n (numberOfRiskFactors) "largest"
% risk faktorts out of the forward rate curve (forwardMatrix) by PCA and
% determines how these explains every forward curve in the forward matrix
% using linear regression.
%
% Inputs:
%   numberOfRiskFactors [integer] - number of risk factors get in output
%   forwardMatrix [matrix] - matrix containing forward rates           
%
% Outputs:
%   riskFactors [matrix] - all numberOfRiskFactors risk factors that 
% discribes the changes in the forward matrix.
%   explanation [vector] - how much each risk factor explains the total
% correlation of the forward matrix
%   changeInForwardRate [matrix] - a linear combination of the risk factors that
% explains the forward curve.
%   dailyChangeInForwardRate [matrix] - the change in forward rate from day
%   to day.
%
% Help functions:
%    none
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Johan Hyd�n
% November 2014; Last revision: 23-11-2014
    
    for i=1:size(forwardMatrix,2)-1 
        forwardDiff(:,i)=forwardMatrix(:,i+1)-forwardMatrix(:,i);
    end
    disp('Size');
    disp(size(forwardDiff));

    size(forwardDiff)
    forwardMatrix=forwardMatrix(:, 1:end-1);
    size(forwardMatrix)
    % PCA
    [riskFactors, ~, explanation]=pcacov(forwardDiff);
    
    % Only using the numberOfRiskFactors "largest" risk factors
    riskFactors=riskFactors(:,1:numberOfRiskFactors);
    explanation=explanation(1:numberOfRiskFactors);
    
    % Initializing the size of the changeInForwardRate-matrix and
    % dailyChangeInForwardRate-matrix
    changeInForwardRate=zeros(size(forwardMatrix,1),numberOfRiskFactors);
    dailyChangeInForwardRate=zeros(size(forwardMatrix,1),numberOfRiskFactors);
    
    % Using linear regression to determine the change using only risk
    % factor for every forward curve in the forward matrix
    for i=1:size(forwardMatrix,1)
        changeInForwardRate(i,:)=regress(forwardMatrix(i,:)',riskFactors)';
    end
    
    % Daily change is the change in the forward rate from day to day
        dailyChangeInForwardRate(1,:)=changeInForwardRate(1,:);
    for i=2:size(forwardMatrix,1)
        dailyChangeInForwardRate(i,:)=changeInForwardRate(i,:)-changeInForwardRate(i-1,:);
    end
    
    % Plots the risk factors
%     for i=1:numberOfRiskFactors
%         hold on
%         plot(riskFactors(:,i))
%     end
%     
%      hold off
end

