function bDay = ceilToBusinessDayImproved(date)

dateVector = evalin('base', 'businessDayImprovedList');

%Uses dateMap and dayList to increase performance
if (date < 726835)
  error('date is before the beginning of our timecount...')
elseif date <= 735902
  bDay = dateVector(date-726834);
else
  bDay = date;
end

end
  