
function [e,sV,C] = pcaAnalysis(v, returns)

if (strcmp(returns,'diff'))
  r = v(2:end,:)-v(1:end-1,:);
elseif (strcmp(returns,'arith'))
  r = v(2:end,:)./v(1:end-1,:)-1;
elseif (strcmp(returns,'log'))
  r = log(v(2:end,:)./v(1:end-1,:));
else
  error('unknown return type');
end

C = cov(r);

[V,D] = eigs(C, 10);
[e,ind] = sort(diag(D),1, 'descend');
sV = V(:,ind);