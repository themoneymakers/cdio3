%  dailyf = forwardMatrix;
%   % remove dates which do not have 10-years of data (and contains zeros)
%   remInd = find(dailyf(:,end) == 0);
% 
%   remInd
%   dailyf(remInd,:) = [];
%   
  cf = [zeros(size(dailyf,1),1) cumsum(dailyf,2)];
  indM = round((1:120)/12*365);
  fM = (cf(:,indM)-cf(:,[1 indM(1:end-1)]))./repmat((indM - [1 indM(1:end-1)]),size(dailyf,1),1); % 1 month
  
  [fe,fV,fC] = pcaAnalysis(fM,'diff');


