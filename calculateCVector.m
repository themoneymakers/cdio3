function cVector = calculateCVector(interestRateInstrument, valuationDate)

nominalValue = interestRateInstrument.nominalValue;
yield = interestRateInstrument.yield;
maturityDate = interestRateInstrument.maturityDate;

if isa(interestRateInstrument,'ForwardRateAgreement')
    % forward rate agreement

    if (maturityDate < valuationDate)
        error('maturityDate < valuationDate')
    end

    cVector = zeros(2,1);

    terminationDate = interestRateInstrument.terminationDate;

    timeCountTcFra = (terminationDate-maturityDate)/360;

    cVector(1) = -nominalValue;
    cVector(2) = nominalValue*(1+yield*timeCountTcFra);

else    
    % interest rate swaps
    fixDates = interestRateInstrument.fixDates;
    floatDates = interestRateInstrument.floatDates;
    issueDate = interestRateInstrument.issueDate;

    if(valuationDate > max(fixDates(end),floatDates(end)))
        error('Valuation Date greater than Maturity Date')
    end

    % floating leg

    [timeCountFloat timeForLastFixing] = calculateTimeCountFloat(valuationDate, floatDates);    
    LIBOR3M = evalin('base', 'LIBOR3M');
    if (timeForLastFixing == 0)
        timeForLastFixing = issueDate;
    end
    
    if (isKey(LIBOR3M,timeForLastFixing))        
        yieldFloat = LIBOR3M(timeForLastFixing);

    % fixed leg

    [timeCountFix numberOfFixPayments] = calculateTimeCountFix(valuationDate, fixDates);

    % C

    cVector = zeros(numberOfFixPayments+1,1);

    %float
    cVector(1) = nominalValue*(1+yieldFloat*timeCountFloat);

    %fic
    for i = 2:numberOfFixPayments
        cVector(i)= -nominalValue*yield*timeCountFix(i-1);
    end

    cVector(numberOfFixPayments+1)=-nominalValue*(1+yield*timeCountFix(numberOfFixPayments));
    
    else
        
    cVector = 0;
    
    end
    
end


end

function [timeCountFloat timeForLastFixing] = calculateTimeCountFloat(valuationDate, paymentDates)
% Calculates needed date and time for floating leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] - containing dates for all floating payments
%
% Outputs
%   timeCountFloat - Proper time count for next floating payment
%   timeForNextFloatingPayment - The date of the next floating payment
%   timeForLastFixing - The date of the fixing of the floating rate 

%------------- BEGIN CODE -----------------------------------------------

for i = 1:length(paymentDates)
    timeToAllPayments(i) = (paymentDates(i) - valuationDate)/360;
end

timeCountFloat = timeToAllPayments(timeToAllPayments>0);
timeForAllFloatingPayment = paymentDates(timeToAllPayments>0);
timeCountFloat = timeCountFloat(1);

if (length(paymentDates) == length(timeForAllFloatingPayment))
    timeForLastFixing = 0;
else
    timeForLastFixing = paymentDates(timeToAllPayments<=0);
    timeForLastFixing = timeForLastFixing(end);
end
    

end

%------------- END CODE -------------------------------------------------

function [timeCountFix numberOfFixPayments] = calculateTimeCountFix(valuationDate, paymentDates)
% Calculates dates and times for fixed leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] -  containing dates for all fixed payments
%
% Outputs
%   timeCountFix - proper time count for all fixed payments
%   timeForFixPayments - the dates for all future payments 
%   numberOfFixPayments - total number of fixed payments

%------------- BEGIN CODE -----------------------------------------------

numberOfPayments = length(paymentDates);

for i = 1:numberOfPayments
    testTime(i) = paymentDates(i)-valuationDate;
end

timeForFixPayments = paymentDates(testTime>0);
numberOfFixPayments=length(timeForFixPayments);

dateVectorValuation = datevec(valuationDate);
if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
end

for j=1:numberOfFixPayments
    dateVectorFix = datevec(timeForFixPayments(j));
    if (j == 1)
        dateVectorValuation = datevec(valuationDate);
    else
        dateVectorValuation = datevec(timeForFixPayments(j-1));
    end
    if dateVectorFix(3) == 31
        dateVectorFix(3) = 30;
    end
    if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
    end
    timeCountFix(j) = 1/360*(360*(dateVectorFix(1)-dateVectorValuation(1))+30*(dateVectorFix(2)-dateVectorValuation(2))+(dateVectorFix(3)-dateVectorValuation(3)));
end

end
