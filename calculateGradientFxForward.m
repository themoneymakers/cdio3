function [gradientVector] = calculateGradientFxForward(elementFx, riskFactorEigenMatrix, valuationNumber, spotRateMatrix, valuationDate, businessDayList)
%function [gradientVector] = calculateGradientFxForward(elementFx, riskFactorDomestic, riskFactorevaluationDate, )
%
%

% a(T)*T*e^r(t)t

maturityDate = elementFx.maturityDate;
[spotRate timeToMaturity] = findSpotRateAndTime(valuationNumber, spotRateMatrix, valuationDate, maturityDate);

startDate = max(interestRateInstrument.spotDate, valuationDate);

lengthK = calculateNumberOfBusinessDayBetweenDatesImproved(maturityDate, startDate, businessDayList)

%vad ska in i riskFactorEigenMatrix?
gradientVector = riskFactorEigenMatrix(VADSKAINHAR,:)riskFactorEigenMatrix(VADSKAINHAR,:)'*timeToMaturity^2*exp(spotRate*timeToMaturity);


end

function [spotRate timeToMaturity] = findSpotRateAndTime(valuationNumber, spotRateMatrix, valuationDate, maturityDate)
% Finds spot rate and time to maturity 
%Uses the spot rate matrix

%
%------------- BEGIN CODE --------------

valuationLength = maturityDate - valuationDate;
timeToMaturity = valuationLength/365;
spotRate = spotRateMatrix(valuationNumber,valuationLength);

end

%------------- END CODE --------------