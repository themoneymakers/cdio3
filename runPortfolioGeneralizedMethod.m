startTime = now;
datestr(startTime)
portfolioStartDate= 2450;
distanceFromPortfolioStart =100; %250;
Portfolio=PortfolioFRAIRSW;

%Is it a FX portfolio?
FX=false; % Var satt till true? 

%Lags the estimates of mean/covariance of risk factors
meanLag = 60;

endDate=6216;

%Uses combined businessDayList for FX portfolios
if FX
    runDays = businessDayListCombined(portfolioStartDate+distanceFromPortfolioStart:endDate);
else
    runDays = businessDayList(portfolioStartDate+distanceFromPortfolioStart:endDate);
end
% runDays = businessDayList((2450+1315):6216); % Crashes after 28-jan-2005 (732339 (runDays(1318)) ), so changed the run period

VAR = zeros(length(runDays),1);
ES = zeros(length(runDays),1);
% priceDifference = zeros(length(runDays), 1);
% priceDifference=priceDifferenceInPortfolio(Portfolio,businessDayList, businessDayList(portfolioStartDate+distanceFromPortfolioStart), businessDayList(endDate));
priceDifference = createPortfolioReturnsMap( Portfolio, runDays ); % Actually creates a vector
VAR(1:2,1)=ones(2,1);
tradVAR = zeros(length(runDays), 1);
tradVAR(1:2,1)= ones(2,1);
numberOfMisses = 0;
numberOfMissesTrad = 0;
exceedDates= [];
exceedDatesTrad= [];

for generalIterator=1:length(runDays) % evalin-ing the iterator to simulateGen... so preferred something other than i //Pontus 
    tic
    tradVAR(generalIterator, 1) = calculateVaRTraditional(Portfolio, runDays(generalIterator), runDays(generalIterator));
    toc
    tradVAR(generalIterator, 1)
    
    [VAR(generalIterator) ES(generalIterator)]= runPortfolioOneDay(Portfolio, runDays(generalIterator));
%     VAR(generalIterator)= runPortfolioOneDay(Portfolio, runDays(generalIterator));
    VAR(generalIterator)
    %ES(generalIterator)
    if -VAR(generalIterator)>=priceDifference(generalIterator)
      numberOfMisses=numberOfMisses+1;
      exceedDates = [exceedDates generalIterator];
    end

    if -tradVAR(generalIterator)>=priceDifference(generalIterator)
      numberOfMissesTrad=numberOfMissesTrad+1;
      exceedDatesTrad = [exceedDatesTrad generalIterator];
    end
    
    generalIterator
    meanVector = mean(dailyChange(portfolioStartDate+distanceFromPortfolioStart+generalIterator:portfolioStartDate+distanceFromPortfolioStart+meanLag+generalIterator,:));
    covarianceMatrix = cov(dailyChange(portfolioStartDate+distanceFromPortfolioStart+generalIterator:portfolioStartDate+distanceFromPortfolioStart+meanLag+generalIterator,:));
    
    pause(0.02)
end

VAR

endTime = now;
datestr(startTime)
datestr(endTime)

